<?php if ( ! defined('BASEPATH')) return('No direct script access allowed');

class Install extends CI_Controller {

	public function __construct()
	{
		//parent::CI_Controller();
		parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        //$this->load->library('zebra_pagination');
	}

    /**
     * Applications landing page
     *
     * @param none
     * @return void
     */
    public function index()
	{
        $data='';
        $this->display_install($data);
	}

    /**
     * Applications list page theme
     *
     * @param array $theme_data  user_info
     * @return void
     */
    private function display_install($theme_data=array())
    {
        $output='';
        $message='';
        $data='';
	    $theme_data=[];
        //$user_info = $theme_data['user_info'];

        //get users dir list
        $users_path = 'C:/Users';
        $user_dirs = array();
        // directory handle
        $dir = dir($users_path);
        while (false !== ($entry = $dir->read())) {
            if ($entry != '.' && $entry != '..' && $entry != 'Default' && $entry != 'Public') {
                if (is_dir($users_path . '/' .$entry)) {
                    $user_dirs[] = $users_path . '/' .$entry;
                }
            }
        }
	    //print_r($user_dirs);
	    //exit;
        $theme_data['user_dirs'] = $user_dirs;
        //print_r($user_dirs);
        //

        //get vhost location
        $wamp_path = $this->_get_wamp_path();
        $theme_data['vhosts_path'] = '';
        if($wamp_path != '') {
            $theme_data['vhosts_path'] = $wamp_path.'/apache2/conf/extra/httpd-vhosts.conf';;
        }
        //print_r($user_dirs);
        //

        //print $output;
        $theme_data['title'] = 'Artlivemedia';
        $theme_data['install_form'] = $this->load->view("install_form_tpl",$theme_data,TRUE);
        $theme_data['delete_project_form'] = $this->load->view("delete_project_form_tpl",$theme_data,TRUE);
        //$theme_data['left_sidebar'] = $this->load->view('left_sidebar_tpl',$theme_data,TRUE);
        //$theme_data['v_help_cat'] = 'applications';
        $theme_data['header'] = $this->load->view('header_tpl',$theme_data,TRUE);
        $theme_data['footer'] = $this->load->view('footer_tpl',$theme_data,TRUE);
        $this->load->view('install_tpl', $theme_data);
    }

    public function _get_wamp_path()
    {
        //get vhost location
        $wamp_path = '';
        $wamp_loop_path = 'C:';
        $user_dirs = array();
        $dir = dir($wamp_loop_path);
        while (false !== ($entry = $dir->read())) {
            if ($entry != '.' && $entry != '..') {
                if (strtolower($entry) == 'bitnami') {
                    $wamp_loop_path = $wamp_loop_path .'/'. $entry;
	                //$wamp_path = $wamp_loop_path.'/'. $entry;
	                $dir = dir($wamp_loop_path);
                }
                if (strstr(strtolower($entry), 'wampstack')) {
                	//echo $entry;
                    //$wamp_loop_path = $wamp_loop_path .'/'. $entry;
	                //$wamp_path = $wamp_loop_path; //.'/apache2/conf/extra/httpd-vhosts.conf';
	                $wamp_path = $wamp_loop_path.'/'. $entry;
                    //$dir = dir($wamp_path);
                    //break;
                }
            }
        }
        //

        return $wamp_path;
    }

    public function _get_latest_alm_default_wp_folder_name($path)
    {
        //get vhost location
        $folder_name = '';
        $dir = dir($path);
        while (false !== ($entry = $dir->read())) {
            if ($entry != '.' && $entry != '..') {
                if (strstr(strtolower($entry), 'artlivemediacode-alm-wordpress-default-theme')) {
                    $folder_name = $entry;
                    //$dir = dir($wamp_path);
                    break;
                }
            }
        }
        //

        return $folder_name;
    }

    /**
     * Create/Update Applications
     *
     * @param none
     * @return string json
     */
    public function run()
    {
        $time = time();
        $data['success'] = false;
        $output=$msg='';

        if(!IS_AJAX) {
            $this->common->page_not_found();
        };

        ini_set('max_execution_time', 300000); //300 seconds = 5 minutes

        /*if(!$this->common->is_valid_session()) {
            $data['requires_login'] = TRUE;
            $this->common->ajaxResponse($output,'',$data);
            return;
        }*/

        ///VALIDATION
        $this->form_validation->set_rules('project_name', 'project_name', 'trim|required');
        $this->form_validation->set_rules('project_path', 'project_path', 'trim|required');
        $this->form_validation->set_rules('localhost', 'localhost', 'trim|required');
        $this->form_validation->set_rules('vhosts', 'vhosts', 'trim|required');
        $this->form_validation->set_rules('build', 'build', 'trim|required');
        //$this->form_validation->set_rules('hosting', 'hosting', 'trim|required');
        $this->form_validation->set_rules('is_wpengine', 'is_wpengine', 'trim');
        $this->form_validation->set_rules('is_git', 'is_git', 'trim|required');
        //$this->form_validation->set_rules('user_directory', 'user_directory', 'trim');
        if(IS_AJAX && $this->form_validation->run() === FALSE) {
            $data['error_msg']['project_name'] = form_error('project_name');
            $data['error_msg']['project_path'] = form_error('project_path');
            $data['error_msg']['localhost'] = form_error('localhost');
            $data['error_msg']['vhosts'] = form_error('vhosts');
            $data['error_msg']['build'] = form_error('build');
            //$data['error_msg']['hosting'] = form_error('hosting');
            $data['error_msg']['is_wpengine'] = form_error('is_wpengine');
            $data['error_msg']['is_git'] = form_error('is_git');
            $this->common->ajaxResponse('','',$data);
            return;
        }
        //VALIDATION END

        if($this->input->post('is_wpengine') == 'yes' && $this->input->post('is_git') == 'yes') {
            if($this->input->post('user_directory') == '') {
                $data['error_msg']['user_directory'] = 'Please select user directory';
                $this->common->ajaxResponse('','',$data);
                return;
            }

            if($this->input->post('email') == '') {
                $data['error_msg']['email'] = 'Please enter your email.';
                $this->common->ajaxResponse('','',$data);
                return;
            }
        }

        $project_name = trim($this->input->post('project_name'));
        $project_path = trim($this->input->post('project_path'));
        $project_root_path = $project_path.'/'.$project_name;

        //echo $this->input->post('vhosts');
        //exit;
        //Check hosts file
        $hosts_file = "C:/Windows/System32/drivers/etc/hosts";
        if(!file_exists($hosts_file) || !is_writable($hosts_file)) {
            $data['error_msg']['localhost'] = 'hosts file not found in '.$hosts_file;
            $this->common->ajaxResponse('','',$data);
            return;
        }
        //

        //Check vhosts file
        $vhosts_file = $this->input->post('vhosts');
        if(!file_exists($vhosts_file) || !is_writable($vhosts_file)) {
            $data['error_msg']['vhosts'] = 'vhosts file not found in '.$vhosts_file;
            $this->common->ajaxResponse('','',$data);
            return;
        }
        //

        /*//Check project folder perms
        $project_path = $this->input->post('project_path');
        if(!file_exists($project_path) || !is_writable($project_path)) {
            $data['error_msg']['vhosts'] = 'vhosts file not found in '.$vhosts_file;
            $this->common->ajaxResponse('','',$data);
            return;
        }
        //*/

        //Update hosts file //todo RAJA check if already added
        $txt = "127.0.0.1   ".$this->input->post('localhost');
        $hosts_cnt = file_get_contents($hosts_file);
        if(strstr($hosts_cnt, $this->input->post('localhost'))) {
        } else {
            $hosts_res = file_put_contents($hosts_file, PHP_EOL . $txt . PHP_EOL, FILE_APPEND);
            if ($hosts_res == false) {
                $data['error_msg']['localhost'] = 'Could not write hosts file. Please check permissions.'; //.$txt;
                $this->common->ajaxResponse('', '', $data);
                return;
            }
        }
        //

        /*//create project directory
        if(!file_exists($project_path.'/'.$project_name)) {
            mkdir($project_path.'/'.$project_name);
        }
        //*/

        //Update httpd-vhosts file
        $vhosts_cnt = file_get_contents($vhosts_file);
        if(strstr($vhosts_cnt, $this->input->post('localhost'))) {
        } else {
            $f_data['doc_root'] = $project_path.'/'.$project_name;
            //$f_data['project_name'] = $project_name;
            $f_data['localhost'] = $this->input->post('localhost');
            $f_data['localhost_alias'] = $this->input->post('localhost');
            $vhosts_txt = $this->load->view('virtual_host_tpl', $f_data, TRUE);
            $vhosts_res = file_put_contents($vhosts_file, PHP_EOL . $vhosts_txt . PHP_EOL, FILE_APPEND);
            if ($vhosts_res == false) {
                $data['error_msg']['localhost'] = 'Could not write vhosts file. Please check permissions.'; //.$txt;
                $this->common->ajaxResponse('', '', $data);
                return;
            }
        }
        //

        //Update ssh config file
        if($this->input->post('is_wpengine') == 'yes' && $this->input->post('is_git') == 'yes') {
            //Check ssh config file
            $ssh_dir = $this->input->post('user_directory').'/.ssh/';
            $ssh_file = $ssh_dir.'config';
            if(!file_exists($ssh_dir)) {
                mkdir($ssh_dir);
            }
            if(!file_exists($ssh_file)) {
                touch($ssh_file);
            }
            //

            $ssh_cnt = file_get_contents($ssh_file);
            if(strstr($ssh_cnt, $project_path.'\\'.$project_name)) {
            } else {
                $f_data['project_name'] = $project_name;
                $f_data['project_path'] = $project_path;
                $ssh_config_txt = $this->load->view('wpengine_ssh_config_tpl', $f_data, TRUE);
                $ssh_config_res = file_put_contents($ssh_file, PHP_EOL . $ssh_config_txt . PHP_EOL, FILE_APPEND);
                if ($ssh_config_res == false) {
                    $data['error_msg']['localhost'] = 'Could not write ssh config file. Please check permissions.'; //.$txt;
                    $this->common->ajaxResponse('', '', $data);
                    return;
                }
            }
        }
        //

        //check if project folder already exists //TODO
        if(file_exists($project_root_path)) {
            $data['error_msg']['project_name'] = 'Project folder already exists. Please delete and try again.'; //.$txt;
            $this->common->ajaxResponse('', '', $data);
            return;
        }
        //

        //
        if($this->input->post('build') == 'wordpress') {
            /*$cmd = "cd ".APPPATH."tmp";
            $cmd .= " && ";
            $cmd .= "git clone https://github.com/WordPress/WordPress.git";
            $output = shell_exec($cmd);*/

            $wp = "http://wordpress.org/latest.zip";
            //$dest = APPPATH."tmp/wordpress.zip";
            $dest = $project_path."/wordpress.zip";
            if(file_exists($dest)) {
                unlink($dest); //TODO
            }
            $res = copy($wp, $dest); //TODO
            if(file_exists($project_path."/wordpress.zip")) {
                if(file_exists($project_root_path)) {
                } else {                //system('unzip '.$dest.' '.APPPATH."tmp/");
                    //$cmd = "rm -r ".APPPATH."tmp\\wordpress";
                    //$o = `$cmd`;
                    //exec($cmd);

                    $zip = new ZipArchive;
                    $res = $zip->open($dest);
                    //$zip->extractTo(APPPATH."tmp/");
                    $zip->extractTo($project_path);
                    $zip->close();
                    //echo "WOOT! $file extracted to $path";
                    //$cmd = "mv ".APPPATH."tmp\\wordpress ".$project_path.'\\'.$project_name;
                    //exec($cmd);

                    //@copy($project_path."/wordpress", $project_path.'/'.$project_name);
                    //@copy(APPPATH."tmp/wordpress", $project_path.'/'.$project_name);
                    //@rename(APPPATH."tmp/wordpress", $project_path.'/'.$project_name);
                    $res = rename($project_path . "/wordpress", $project_path . '/' . $project_name);
                    //$this->copy_directory(APPPATH."tmp/wordpress", APPPATH."tmp/wordpress")
                }
                if(file_exists($project_root_path)) {
                    $db_name = 'wordpress_'.$project_name;
                    $this->_create_wp_database($db_name);
                }
            }
        } elseif($this->input->post('build') == 'wordpress_use_alm_theme') {
            /*$cmd = "cd ".APPPATH."tmp";
            $cmd .= " && ";
            $cmd .= "git clone https://rajamunisamy@bitbucket.org/artlivemediacode/alm-wordpress-default-theme.git";
            $output = shell_exec($cmd);*/
            //rmdir(APPPATH."tmp/wordpress");

            $dest = $project_path . "/wordpressalmdefault.zip";
            if(file_exists($dest)) {
                unlink($dest); //TODO
            }
            if(!file_exists($project_path."/wordpressalmdefault.zip")) {
                //$wp = "https://bitbucket.org/artlivemediacode/alm-wordpress-default-theme/get/f309ec20d0d5.zip";
                $wp = "https://bitbucket.org/artlivemediacode/alm-wordpress-default-theme/get/HEAD.zip";
                //$dest = APPPATH."tmp/wordpressalmdefault.zip";
                $res = copy($wp, $dest); //TODO
            }
            if(file_exists($project_path."/wordpressalmdefault.zip")) {
                if(file_exists($project_root_path)) {
                } else {
                    /*$dest = escapeshellarg($dest);
                    $project_path = escapeshellarg($project_path);*/
                    /*$dest = str_replace(" ", "\\ ", $dest);
                    $project_path = str_replace(" ", "\\ ", $project_path);*/

                    /*echo $dest;
                    echo $project_path;*/

                    $zip = new ZipArchive;
                    $res = $zip->open($dest );
                    //$zip->extractTo(APPPATH."tmp/");
                    $zip->extractTo( $project_path );
                    $zip->close();
                    //echo "WOOT! $file extracted to $path";
                    //$cmd = "mv ".APPPATH."tmp\\wordpress ".$project_path.'\\'.$project_name;
                    //exec($cmd);
                    //exit;

                    $alm_default_wp_folder = $this->_get_latest_alm_default_wp_folder_name($project_path);
                    if ($alm_default_wp_folder == '') {
                        $data['error_msg']['Build'] = 'Could not copy the theme files. Unknown error occurred.'; //.$txt;
                    } else {
                        //@copy($project_path."/wordpress", $project_path.'/'.$project_name);
                        //@copy(APPPATH."tmp/wordpress", $project_path.'/'.$project_name);
                        //@rename(APPPATH."tmp/wordpress", $project_path.'/'.$project_name);
                        //@copy($project_path."/almwpdefault", $project_path.'/'.$project_name);
                        $res = rename($project_path . "/" . $alm_default_wp_folder, $project_path . '/' . $project_name);
                        //@copy($project_path."/almwpdefault", $project_path.'/'.$project_name);
                        if ($res) {

                        }
                    }
                }

                if(file_exists($project_root_path)) {
                    $db_name = 'wordpress_' . $project_name;
                    $sql_path = $project_root_path . '/sql.sql';
                    $this->_create_wp_database($db_name);
                    $this->_import_default_wp_sql($db_name, $sql_path);
                }
            }
        } else {
            //create project directory
            if(!file_exists($project_path.'/'.$project_name)) {
                mkdir($project_path.'/'.$project_name);
            }
            //
        }
        //

        //if its wordpress update wp-config and create db
        if(file_exists($project_path.'/'.$project_name.'/wp-config-sample.php')) {
            if(!file_exists($project_path.'/'.$project_name.'/wp-config.php')) {
                $res = copy($project_path.'/'.$project_name.'/wp-config-sample.php', $project_path.'/'.$project_name.'/wp-config.php');
            }

            $db_name = 'wordpress_'.$project_name;
            $wp_config = $project_path . '/' . $project_name.'/wp-config.php';
            $this->replace_line_in_a_file($wp_config, "define('DB_NAME'", "define('DB_NAME','".$db_name."');");
            $this->replace_line_in_a_file($wp_config, "define('DB_USER'", "define('DB_USER','".$this->db->username."');");
            $this->replace_line_in_a_file($wp_config, "define('DB_PASSWORD'", "define('DB_PASSWORD','".$this->db->password."');");
            $wpconfig_cnt = file_get_contents($wp_config);
            if(stristr($wpconfig_cnt, "define('WP_HOME'")) {
                $this->replace_line_in_a_file($wp_config, "define('WP_HOME'", "define('WP_HOME','".$this->input->post('localhost')."');");
            } else {
                //$wpconfig_res = file_put_contents($wp_config, PHP_EOL . "define('WP_HOME','".$this->input->post('localhost')."');" . PHP_EOL, FILE_APPEND);
            }
            if(stristr($wpconfig_cnt, "define('WP_SITEURL'")) {
                $this->replace_line_in_a_file($wp_config, "define('WP_SITEURL'", "define('WP_SITEURL','".$this->input->post('localhost')."');");
            } else {
                //$wpconfig_res = file_put_contents($wp_config, "define('WP_SITEURL','".$this->input->post('localhost')."');" . PHP_EOL, FILE_APPEND);
            }
            /*if ($wpconfig_res == false) {
                $data['error_msg']['localhost'] = 'Could not write vhosts file. Please check permissions.'; //.$txt;
                $this->common->ajaxResponse('', '', $data);
                return;
            }*/

            /*$this->load->dbforge();
            $this->load->dbutil();
            if (!$this->dbutil->database_exists($db_name)) {
                $this->dbforge->create_database($db_name);

            }*/
        }
        //

        //
        if($this->input->post('is_git') == 'yes') {
            if(file_exists($project_path . '/' . $project_name)) {
                /*$cmd = "cd ".$project_path . '/' . $project_name;
                $cmd .= " && ";
                $cmd .= "git init .";
                $output = shell_exec($cmd);*/
                $cmd = "git init ".$project_path . '/' . $project_name;
                $output = shell_exec($cmd);
            }

            if($this->input->post('is_wpengine') == 'yes') {
                if(file_exists($project_path . '/' . $project_name)) {
                    //create ssh folder inside project
                    $project_ssh_path = $project_path . '/' . $project_name . '/ssh';
                    $project_ssh_file = $project_ssh_path . '/ssh';
                    if (!file_exists($project_ssh_path)) {
                        mkdir($project_ssh_path);
                    }
                    //

                    //add git remote
                    $cmd = substr($project_path,0,2);
                    $cmd .= " && ";
                    $cmd .= "cd ".$project_path . '/' . $project_name;
                    $cmd .= " && ";
                    //$cmd .= "git init .";
                    //$cmd .= " && ";
                    $cmd .= "git remote add ".$project_name."-staging git@wpe_".$project_name.":staging/".$project_name.".git"; // git remote add origin https://rajamunisamy@bitbucket.org/artlivemediacode/".$project_name.".git
                    $output = shell_exec($cmd);
                    //

                    //create ssh key
                    $cmd = 'ssh-keygen -t rsa -b 4096 -C "'.$this->input->post('is_wpengine').'" -f "'.$project_ssh_file.'" -P ""'; //"git remote add ".$project_name."-staging git@wpe_".$project_name.":staging/".$project_name.".git"; // git remote add origin https://rajamunisamy@bitbucket.org/artlivemediacode/".$project_name.".git
                    $output = shell_exec($cmd);
                    //

                }
            }
        }
        //

/*        //
        if($this->input->post('is_wpengine') == 'yes' && $this->input->post('is_git') == 'yes') {

        }
        //*/

        //restart bitnami
        $wamp_path = $this->_get_wamp_path();
        //"C:\Bitnami\wampstack-5.5.26-0\apache2\bin\httpd.exe" -k runservice
        $httpd_path = $wamp_path.'/apache2/bin/httpd.exe';
        $bitnami_mngr_path = $wamp_path.'/manager-windows.exe';
        //$output = shell_exec($httpd_path.' -k restart');
        //echo "<pre>$output</pre>";
        //

        $data['success'] = true;
        $data['bitnami_mngr_path'] = $bitnami_mngr_path;
        $data['localhost'] = $this->input->post('localhost');
        $this->common->ajaxResponse($output,$msg,$data);
    }

    /**
     * Remove Poject from a specified path
     *
     * @param none
     * @return string json
     */
    public function delete_project()
    {
        $time = time();
        $data['success'] = false;
        $output = $msg = '';

        if (!IS_AJAX) {
            $this->common->page_not_found();
        };

        ini_set('max_execution_time', 300000); //300 seconds = 5 minutes

        /*if(!$this->common->is_valid_session()) {
            $data['requires_login'] = TRUE;
            $this->common->ajaxResponse($output,'',$data);
            return;
        }*/

        ///VALIDATION
        $this->form_validation->set_rules('project_name', 'project_name', 'trim|required');
        $this->form_validation->set_rules('project_path', 'project_path', 'trim|required');
        $this->form_validation->set_rules('localhost', 'localhost', 'trim|required');
        $this->form_validation->set_rules('vhosts', 'vhosts', 'trim|required');
        if (IS_AJAX && $this->form_validation->run() === FALSE) {
            $data['error_msg']['project_name'] = form_error('project_name');
            $data['error_msg']['project_path'] = form_error('project_path');
            $data['error_msg']['localhost'] = form_error('localhost');
            $data['error_msg']['vhosts'] = form_error('vhosts');
            $this->common->ajaxResponse('', '', $data);
            return;
        }
        //VALIDATION END

        if ($this->input->post('is_wpengine') == 'yes' && $this->input->post('is_git') == 'yes') {
            if ($this->input->post('user_directory') == '') {
                $data['error_msg']['user_directory'] = 'Please select user directory';
                $this->common->ajaxResponse('', '', $data);
                return;
            }
        }

        $project_name = trim($this->input->post('project_name'));
        $project_path = trim($this->input->post('project_path'));
        $project_root_path = $project_path . '/' . $project_name;

        //check if project folder exists //TODO
        if(file_exists($project_root_path)) {
            $data['error_msg']['project_name'] = 'Project folder not found. Please try again.'; //.$txt;
            $this->common->ajaxResponse('', '', $data);
            return;
        }
        //
    }

    function _create_wp_database($db_name) {
        $this->load->dbforge();
        $this->load->dbutil();
        if (!$this->dbutil->database_exists($db_name)) {
            $this->dbforge->create_database($db_name);

        }
    }

    function _import_default_wp_sql($db_name, $sql_path) {
        $this->load->dbforge();
        $this->load->dbutil();
        if (!$this->dbutil->database_exists($db_name)) {
            return false;
        }
        //$this->db->database = $db_name;

        $config['hostname'] = $this->db->hostname;
        $config['username'] = $this->db->username;
        $config['password'] = $this->db->password;
        $config['database'] = $db_name;
        $config['dbdriver'] = "mysqli";
        $config['dbprefix'] = "";
        $config['pconnect'] = FALSE;
        $config['db_debug'] = TRUE;
        $config['cache_on'] = FALSE;
        $config['cachedir'] = "";
        $config['char_set'] = "utf8";
        $config['dbcollat'] = "utf8_general_ci";
        $db = $this->load->database($config, TRUE);

        /*$sql_source = file_get_contents($sql_path);
        mysqli_multi_query($sql, $sql_source);*/
        //$this->replace_line_in_a_file($sql_path, "'siteurl',", "define('WP_HOME','".$this->input->post('localhost')."');");

        /*$op_data = '';
        $lines = file($sql_path);
        foreach ($lines as $line) {
            if (substr($line, 0, 2) == '--' || $line == '') {//This IF Remove Comment Inside SQL FILE
                continue;
            }
            $op_data .= $line;
            if (substr(trim($line), -1, 1) == ';') {//Breack Line Upto ';' NEW QUERY
                //str_replace('almwp', '', $op_data);
                $db->query($op_data);
                $op_data = '';
            }
        }*/

        $wamp_path = $this->_get_wamp_path();
        $mysql_path = $wamp_path.'/mysql/bin/mysql.exe';
        $command = $mysql_path." -u {$this->db->username} -p{$this->db->password} --database={$db_name} < ".escapeshellarg($sql_path);
        exec($command);

        //update site url and home
        $update_vals['option_value'] = 'http://'.$this->input->post('localhost');
        //$update_vals['option_value'] = 'http://'.$this->input->post('localhost');

        $db->flush_cache();
        $db->where('option_name', 'siteurl');
        $db->or_where('option_name', 'home');
        $app_std_update_res = $db->update('wp_options',$update_vals);
        //

        /*$sql = file_get_contents($sql_path);
        $sqls = explode(';', $sql);
        array_pop($sqls);
        foreach($sqls as $statement){
            $statment = $statement . ";";
            $db->query($statement);
        }*/
    }

    function test() {
        $project_ssh_file = 'D:/ssh';
        //

        //add git remote
        $cmd = 'ssh-keygen -t rsa -b 4096 -C "raja@artlivemedia.com" -f "'.$project_ssh_file.'" -P ""'; //"git remote add ".$project_name."-staging git@wpe_".$project_name.":staging/".$project_name.".git"; // git remote add origin https://rajamunisamy@bitbucket.org/artlivemediacode/".$project_name.".git
        $output = shell_exec($cmd);
        //

        exit;
        //global $db;
        //print_r($db);
        echo $this->db->hostname;
        echo $this->db->username;
        echo $this->db->password;
        echo $this->db->database;
        /*$cmd = "F: ";
        $cmd .= " && ";
        $cmd .= "cd F:/MyProjects/2datafish";
        $cmd .= " && ";
        $cmd .= "git init .";
        $cmd .= " && ";
        $cmd .= "git remote add origin https://rajamunisamy@bitbucket.org/artlivemediacode/alm-local-automation.git";
        $output = shell_exec($cmd);*/
        //$this->db->flush_cache();
    }

    function replace_line_in_a_file($file_path, $find, $replace) {
        $tmp_file_path = $file_path.'.tmp';
        $reading = fopen($file_path, 'r');
        $writing = fopen($tmp_file_path, 'w');

        $replaced = false;

        while (!feof($reading)) {
            $line = fgets($reading);
            if (stristr($line,$find)) {
                $line = $replace."\n";
                $replaced = true;
            }
            fputs($writing, $line);
        }
        fclose($reading); fclose($writing);
        // might as well not overwrite the file if we didn't replace anything
        if ($replaced) {
            rename($tmp_file_path, $file_path);
        } else {
            unlink($tmp_file_path);
        }
    }

    function copy_directory( $source, $destination ) {
        if ( is_dir( $source ) ) {
            @mkdir( $destination );
            $directory = dir( $source );
            while ( FALSE !== ( $readdirectory = $directory->read() ) ) {
                if ( $readdirectory == '.' || $readdirectory == '..' ) {
                    continue;
                }
                $PathDir = $source . '/' . $readdirectory;
                if ( is_dir( $PathDir ) ) {
                    copy_directory( $PathDir, $destination . '/' . $readdirectory );
                    continue;
                }
                copy( $PathDir, $destination . '/' . $readdirectory );
            }

            $directory->close();
        }else {
            copy( $source, $destination );
        }
    }

}
/* End of file applications.php */
/* Location: ./application/controllers/applications.php */
