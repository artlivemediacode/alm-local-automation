<?php

/**
 * getHeader
 *
 * Themes header file
 *
 * @access	public
 * @param	mixed
 * @return	string
 */
if ( ! function_exists('getHeader')) {
    function getHeader()
    {
        $CI =& get_instance();
        $data='';
        return $CI->load->view('header_tpl', NULL, TRUE);
    }
}

/**
 * getFooter
 *
 * Themes footer file
 *
 * @access	public
 * @param	mixed
 * @return	string
 */
if ( ! function_exists('getFooter')) {
    function getFooter()
    {
        $CI =& get_instance();
        $data='';
        return $CI->load->view('footer_tpl', $data, TRUE);
    }
}

/**
 * getUserDetailedRoles
 *
 * Themes footer file
 *
 * @access	public
 * @param	mixed
 * @return	string
 */
if ( ! function_exists('_getUserDetailedRoles')) {
    function _getUserDetailedRoles($filt_data = array())
    {
        $CI =& get_instance();
        $CI->load->model('role_model');

        $u_r_filt_data['select'] = 'u_role_id,r_status,roles.role_id,roles.role,roles.role_group,sites.site_id,sites.site_name,
                                        facilities.fac_id,facilities.name as fac_name';
        $u_r_filt_data['get_site_info'] = TRUE;
        $u_r_filt_data['get_fac_info'] = TRUE;
        $u_r_filt_data['user_id'] = isset($filt_data['user_id'])?$filt_data['user_id']:_QPMA_UID;
        if(isset($filt_data['r_status']))
            $u_r_filt_data['r_status'] = $filt_data['r_status'];
        $u_r_res = $CI->role_model->getUserRoles($u_r_filt_data);
        $result = array();
        foreach($u_r_res['user_roles'] as $k => $v) {
            if($v['role_group'] == 'site' || $v['role_group'] == 'qpma') {
                $result[$v['role_group']][$v['role']] = $v;
            } else {
                $result[$v['role_group']][$v['fac_name']][$v['site_name']][$v['role']] = $v;
            }

        }
        //print_r($u_r_res);
        //print_r($result);
        return $result;
    }
}

/**
 * _getUserDetailedSiteRoles
 *
 * Themes footer file
 *
 * @access	public
 * @param	mixed
 * @return	string
 */
if ( ! function_exists('_getUserDetailedSiteRoles')) {
    function _getUserDetailedSiteRoles($filt_data = array())
    {
        $CI =& get_instance();
        $CI->load->model('role_model');

        $u_r_filt_data['select'] = 'u_role_id,r_status,roles.role_id,roles.role,roles.role_group,sites.site_id,sites.site_name,
                                        facilities.fac_id,facilities.name as fac_name';
        $u_r_filt_data['get_site_info'] = TRUE;
        $u_r_filt_data['get_fac_info'] = TRUE;
        $u_r_filt_data['user_id'] = isset($filt_data['user_id'])?$filt_data['user_id']:_QPMA_UID;
        if(isset($filt_data['r_status']))
            $u_r_filt_data['r_status'] = $filt_data['r_status'];
        $u_r_res = $CI->role_model->getUserRoles($u_r_filt_data);
        $result = array();
        foreach($u_r_res['user_roles'] as $k => $v) {
            if($v['role_group'] == 'site' || $v['role_group'] == 'qpma') {
                $result[$v['role_group']][$v['role']] = $v;
            } else {
                $result[$v['role_group']][$v['fac_name']][$v['site_name']][$v['role']] = $v;
            }

        }
        //print_r($u_r_res);
        //print_r($result);
        return $result;
    }
}

/**
 * _getSiteInfo
 *
 * Fetch site info
 *
 * @access	public
 * @param	mixed
 * @return	string
 */
if ( ! function_exists('_getSiteInfo')) {
    function _getSiteInfo($site_id)
    {
        $result = array();

        $CI =& get_instance();
        $CI->load->model('Sites_model');
        $filt_data['site_id'] = $site_id;
        $site_info_res = $CI->Sites_model->getSiteInfo($filt_data);
        $result = (count($site_info_res))?$site_info_res[0]:array();

        return $result;
    }
}

/**
 * getFacilityDataPath
 *
 * Fetch facility assets path
 *
 * @access	public
 * @param	$fac_id
 * @return	string
 */
if ( ! function_exists('_getFacilityDataPath')) {
    function _getFacilityDataPath($fac_id) {
        $data_path = BASEPATH.'../data/';

        $return['abs_path'] = $data_path.'facilities/'.$fac_id;
        $return['rel_path'] = '/data/facilities/'.$fac_id;

        return $return;
    }
}
/**
 * getFacilityDataPath
 *
 * Fetch site assets path
 *
 * @access	public
 * @param	$fac_id
 * @return	string
 */
if ( ! function_exists('_getSiteDataPath')) {
    function _getSiteDataPath($site_id) {
        $data_path = BASEPATH.'../data/';

        $return['abs_path'] = $data_path.'facility_sites/'.$site_id;
        $return['rel_path'] = '/data/facility_sites/'.$site_id;

        return $return;
    }
}

/**
 * _addActivityLog
 *
 * @access	public
 * @param	mixed
 * @return	string
 */
if ( ! function_exists('_addActivityLog')) {
    function _addActivityLog($log_data)
    {
        $result = array();

        $CI =& get_instance();
        //$CI->load->model('Activity_log_model');

        $act_log_res = $CI->db->insert('activity_log', $log_data);
        //$result = (count($site_info_res))?$site_info_res[0]:array();

        return $act_log_res;
    }
}

/**
 * _getVideoHelp
 *
 * Fetch site info
 *
 * @access	public
 * @param	mixed
 * @return	string
 */
if ( ! function_exists('_getVideoHelp')) {
    function _getVideoHelp($v_help_cat)
    {
        $result = array();

        $CI =& get_instance();
        $CI->load->model('Video_help_model');
        $filt_data['cat_slug'] = $v_help_cat;
        $site_info_res = $CI->Video_help_model->getVideoHelpTopics($filt_data);
        //$result = (count($site_info_res))?$site_info_res[0]:array();

        return $site_info_res;
    }
}

function _videoType($url) {
    if (strpos($url, 'youtube') > 0) {
        return 'youtube';
    } elseif (strpos($url, 'vimeo') > 0) {
        return 'vimeo';
    } else {
        return false;
    }
}

function _is_youtube($url)
{
    return (preg_match('/youtu\.be/i', $url) || preg_match('/youtube\.com\/watch/i', $url));
}

function _is_vimeo($url)
{
    return (preg_match('/vimeo\.com/i', $url));
}

function _youtube_video_id($url)
{
  if(_is_youtube($url))
  {
      $pattern = '/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/';
      preg_match($pattern, $url, $matches);
      if (count($matches) && strlen($matches[7]) == 11)
      {
          return $matches[7];
      }
  }

  return '';
}

function _vimeo_video_id($url)
{
    if(_is_vimeo($url))
    {
        $pattern = '/\/\/(www\.)?vimeo.com\/(\d+)($|\/)/';
        preg_match($pattern, $url, $matches);
        if (count($matches))
        {
            return $matches[2];
        }
    }

    return '';
}