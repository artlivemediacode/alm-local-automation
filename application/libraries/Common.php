<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class common {

    /**
     *
     * @param none
     * @return boolean
     */
    function is_valid_session()
    {
        $CI =& get_instance();
        //print_r($_SESSION);

        $v = $CI->session->all_userdata();
        //print_r($v);
        //exit;

        if($CI->session->userdata('user_id')!='' &&
            $CI->session->userdata('auth')==1) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     *
     * @param $path
     * @return url
     */
    static function getUrl($path = '/')
    {
        $CI =& get_instance();

        if ($CI->input->server('HTTPS') == 'on' || $CI->input->server('HTTPS') == 1
            || $CI->input->server('HTTP_X_FORWARDED_PROTO') == 'https') {
            $protocol = 'https://';
        }
        else {
            $protocol = 'http://';
        }

        return $protocol . $CI->input->server('HTTP_HOST') . $path;
    }

    function pageNotFound() {
        $CI =& get_instance();
        $CI->load->view('not_found_tpl');
        return FALSE;
        //todo header
    }

    function userProfileLink($uId)
    {
        return "/profile/$uId";
    }

    function userProfilePic($user_id, $profile_pic, $default_profile_pic='')
    {
        if($default_profile_pic=='')
            $default_profile_pic = config_item('assets_url')."/assets/image/avatar/avatar.png";

        if($profile_pic=='')
            $profile_pic = $default_profile_pic;
        else {
            $uri = substr($profile_pic,0,strrpos($profile_pic,'.')).'_200'.substr($profile_pic,strrpos($profile_pic,'.'));
            //$profile_pic = config_item('http_static_url').'/'.$uri;
            //$profile_pic = $this->get_s3_auth_url($uri);
            $profile_pic = '/data/users/'.$user_id.'/'.$uri;
        }
        return $profile_pic;
    }

    function get_fac_data_path($fac_id) {
        $data_path = BASEPATH.'../data/';

        $return['abs_path'] = $data_path.'facilities/'.$fac_id;
        $return['rel_path'] = '/data/facilities/'.$fac_id;

        return $return;
    }

    function get_site_data_path($site_id) {
        $data_path = BASEPATH.'../data/';

        $return['abs_path'] = $data_path.'sites_fac/'.$site_id;
        $return['rel_path'] = '/data/sites_fac/'.$site_id;

        return $return;
    }

    function getS3AuthUrl($uri, $lifetime='')
    {
        $CI =& get_instance();

        if($lifetime=='')
            $lifetime = config_item('others_s3_lifetime');

        $bucket = config_item('awsBucket');
        $s3_config['access_key'] = config_item('awsAccessKey');
        $s3_config['secret_key'] = config_item('awsSecretKey');
        $CI->load->library('s3', $s3_config);
        //print_r($s3_config);
        $s3_obj = new S3($s3_config);

        //$lifetime = strtotime($lifetime);
        $url = $s3_obj->getAuthenticatedURL($bucket, $uri, $lifetime, $hostBucket = FALSE, $https = TRUE);
        return str_replace('&amp;','&',$url);

    }

    function getS3PublicUrl($uri)
    {
        $s3Key = config_item('awsAccessKey');
        $bucket = config_item('awsBucket');

        $url = "https://{$bucket}.s3.amazonaws.com/{$uri}";
        return $url;
    }

    function getS3FormKeys()
    {
        $result = array();
        $result['bucket'] = $bucket = config_item('awsBucket');
        $s3_key = config_item('awsAccessKey');
        $s3_secret = config_item('awsSecretKey');
        $result['s3_region'] = $s3_region = config_item('awsRegion'); // S3 region name: http://amzn.to/1FtPG6r
        $acl = 'public-read'; //http://amzn.to/18s9Gv7

        $result['algorithm'] = $algorithm = "AWS4-HMAC-SHA256";
        $service = "s3";
        $result['date'] = $date = date('Ymd\THis\Z');
        $short_date = date('Ymd');
        $request_type = "aws4_request";
        $result['expires'] = $expires = '86400'; // 24 Hours

        $scope = [
            $s3_key,
            $short_date,
            $s3_region,
            $service,
            $request_type
        ];
        $result['credentials'] = $credentials = implode('/', $scope);

        $policy = [
            'expiration' => date('Y-m-d\TG:i:s\Z', strtotime('+6 hours')),
            'conditions' => [
                ['bucket' => $bucket],
                ['acl' => $acl],
                [
                    'starts-with',
                    '$key',
                    ''
                ],
                ['success_action_status' => '201'],
                ['x-amz-credential' => $credentials],
                ['x-amz-algorithm' => $algorithm],
                ['x-amz-date' => $date],
                ['x-amz-expires' => $expires],
            ]
        ];
        $base64_policy = base64_encode(json_encode($policy));

        // Signing Keys
        $date_key = hash_hmac('sha256', $short_date, 'AWS4' . $s3_secret, true);
        $date_region_key = hash_hmac('sha256', $s3_region, $date_key, true);
        $date_region_service_key = hash_hmac('sha256', $service, $date_region_key, true);
        $signing_key = hash_hmac('sha256', $request_type, $date_region_service_key, true);

        // Signature
        $result['signature'] = $signature = hash_hmac('sha256', $base64_policy, $signing_key);

        $result['base64_policy'] = $base64_policy;

        return $result;
    }

    /**
     * Truncates the given string at the specified length.
     *
     * @param string $str The input string.
     * @param int $width The number of chars at which the string will be truncated.
     * @param $f_data
     * @return string
     */
    function truncate($str, $width = 20, $f_data = array()) {
        $read_more = (isset($f_data['read_more']))?$f_data['read_more']:"...\n";

        $words = array();
        $words = explode(" ", $str, $width);
        $excerpt = "";

        if(count($words) > $width){
            //$words[$width+1] = '...';
            $result['is_read_more'] = TRUE;
        }

        print_r($words);
        $result['excerpt'] = implode(" ", $words);

        return $result;
        /*return strtok(wordwrap($str, $width, $read_more), "\n");*/
    }

    /**
     * truncateHtml can truncate a string up to a number of characters while preserving whole words and HTML tags
     *
     * @param string $text String to truncate.
     * @param integer $length Length of returned string, including ellipsis.
     * @param string $ending Ending to be appended to the trimmed string.
     * @param boolean $exact If false, $text will not be cut mid-word
     * @param boolean $considerHtml If true, HTML tags would be handled correctly
     *
     * @return string Trimmed string.
     */
    function truncateHtml($text, $length = 100, $ending = '...', $exact = false, $considerHtml = true) {
        if ($considerHtml) {
            // if the plain text is shorter than the maximum length, return the whole text
            if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
                $result['excerpt'] = $text;
                return $result;
            }
            // splits all html-tags to scanable lines
            preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
            $total_length = strlen($ending);
            $open_tags = array();
            $truncate = '';
            foreach ($lines as $line_matchings) {
                // if there is any html-tag in this line, handle it and add it (uncounted) to the output
                if (!empty($line_matchings[1])) {
                    // if it's an "empty element" with or without xhtml-conform closing slash
                    if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
                        // do nothing
                        // if tag is a closing tag
                    } else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
                        // delete tag from $open_tags list
                        $pos = array_search($tag_matchings[1], $open_tags);
                        if ($pos !== false) {
                            unset($open_tags[$pos]);
                        }
                        // if tag is an opening tag
                    } else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
                        // add tag to the beginning of $open_tags list
                        array_unshift($open_tags, strtolower($tag_matchings[1]));
                    }
                    // add html-tag to $truncate'd text
                    $truncate .= $line_matchings[1];
                }
                // calculate the length of the plain text part of the line; handle entities as one character
                $content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
                if ($total_length+$content_length> $length) {
                    // the number of characters which are left
                    $left = $length - $total_length;
                    $entities_length = 0;
                    // search for html entities
                    if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
                        // calculate the real length of all entities in the legal range
                        foreach ($entities[0] as $entity) {
                            if ($entity[1]+1-$entities_length <= $left) {
                                $left--;
                                $entities_length += strlen($entity[0]);
                            } else {
                                // no more characters left
                                break;
                            }
                        }
                    }
                    $truncate .= substr($line_matchings[2], 0, $left+$entities_length);
                    // maximum lenght is reached, so get off the loop
                    break;
                } else {
                    $truncate .= $line_matchings[2];
                    $total_length += $content_length;
                }
                // if the maximum length is reached, get off the loop
                if($total_length>= $length) {
                    break;
                }
            }
        } else {
            if (strlen($text) <= $length) {
                $result['excerpt'] = $text;
                return $result;
            } else {
                $truncate = substr($text, 0, $length - strlen($ending));
            }
        }
        // if the words shouldn't be cut in the middle...
        if (!$exact) {
            // ...search the last occurance of a space...
            $spacepos = strrpos($truncate, ' ');
            if (isset($spacepos)) {
                // ...and cut the text in this position
                $truncate = substr($truncate, 0, $spacepos);
            }
        }
        // add the defined ending to the text
        $truncate .= $ending;
        if($considerHtml) {
            // close all unclosed html-tags
            foreach ($open_tags as $tag) {
                $truncate .= '</' . $tag . '>';
            }
        }
        $result['excerpt'] = $truncate;
        $result['is_read_more'] = TRUE;
        return $result;
    }

    function htpass($pass)
    {
        include(APPPATH.'libraries/password_compat-master/lib/password.php');

        $options = array('cost' => 13);
        $crypt = password_hash($pass, PASSWORD_BCRYPT, $options);

        //return crypt($pass,base64_encode(CRYPT_STD_DES));

        //$crypt = $this->blowfishCrypt($pass,12);

        return $crypt;
    }

    function isValidSession()
    {
        $CI =& get_instance();
        //print_r($_SESSION);

        //$v = $CI->session->all_userdata();
        //print_r($v);
        //exit;

        /*if((isset($_SESSION['auth']) && isset($_SESSION['userInfo'])) &&
            $_SESSION['auth']==1)*/
        ///            $CI->session->userdata('uId')==_ALM_UID &&

        if($CI->session->userdata('user_id')!='' &&
            $CI->session->userdata('auth')==1) {
            return TRUE;
        }

        return FALSE;
    }

    function isSuperAdmin()
    {
        $CI =& get_instance();

        $session_data = $CI->session->all_userdata();
        //print_r($session_data);
        //exit;
        //Super Admin
        if($this->isValidSession() && isset($session_data['cur_role_slug']) && $session_data['cur_role_slug'] == 'super-admin') { //isset($session_data['user_role']['super-admin'])
            return TRUE;
        }
        return FALSE;
    }

    function isUserHavePerm($perm)
    {
        $CI =& get_instance();

        $session_data = $CI->session->all_userdata();
        //echo $perm;
        //print_r($session_data);
        //if(in_array($perm,$session_data['role_perms'])) {
        if(isset($session_data['role_perms'][$perm])) {
            return TRUE;
        }
        return FALSE;
    }
    
    function client_timestamp($time='')
    {
        if($time=='')
            $time = time();

        $CI =& get_instance();
        //echo $time;
        //$offset = ($CI->session->userdata('user_tz_offset')!='')?$CI->session->userdata('user_tz_offset'):0; //DST is not considered in this approach
        //return $time+($offset*60);
        //

        $user_tz = ($CI->session->userdata('user_tz')!='')?$CI->session->userdata('user_tz'):'UTC';
        $date = new DateTime(date('Y-m-d H:i:s',$time));
        $date->setTimezone(new DateTimeZone($user_tz)); //
        return strtotime($date->format('Y-m-d H:i:s'));

        //echo $time+($offset);
        //exit;
    }

    function convertTz($time, $tzone = 'Australia/Queensland')
    {
        $date = new DateTime(null, new DateTimeZone($tzone));
        $date->setTimestamp($time);
        //$date->modify("3 days");
        return $date;
        //echo $date->format("Y-m-d H:i:s\n");
    }

    function tzSelectList($selected_opt='')
    {
        $regions = array(
            'Africa' => DateTimeZone::AFRICA,
            'America' => DateTimeZone::AMERICA,
            'Antarctica' => DateTimeZone::ANTARCTICA,
            'Aisa' => DateTimeZone::ASIA,
            'Atlantic' => DateTimeZone::ATLANTIC,
            'Australia' => DateTimeZone::AUSTRALIA,
            'Europe' => DateTimeZone::EUROPE,
            'Indian' => DateTimeZone::INDIAN,
            'Pacific' => DateTimeZone::PACIFIC
        );

        $timezones = array();
        foreach ($regions as $name => $mask) {
            $zones = DateTimeZone::listIdentifiers($mask);
            foreach($zones as $timezone) {
                $timeOffset_hrs_disp = $this->getGmtOffsetFromTz($timezone);
                $timezones[$name][$timezone] = $timezone." (GMT$timeOffset_hrs_disp)";
            }
        }
        // View
        $output = '';
        foreach($timezones as $region => $list) {
            $output .= '<optgroup label="' . $region . '">' . "\n";
            foreach($list as $timezone => $name) {
                $selected = '';
                if($selected_opt==$timezone)
                    $selected  = 'selected="selected"';
                $output .= '<option value="' . $timezone . '" '.$selected.'>' . $name . '</option>' . "\n";
            }
            $output .= '<optgroup>' . "\n";
        }
        return $output;
    }

    function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        // trim
        $text = trim($text, '-');
        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        // lowercase
        $text = strtolower($text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        if (empty($text)) {
            return '';
        }
        return $text;
    }

    function getGmtOffsetFromTz($timezone, $type="hours")
    {
        if($timezone == '')
            return 0;

        $DateTimeZone = new DateTimeZone($timezone);

        $UTC_DateTimeZone = new DateTimeZone('UTC');
        $UTC_datetime = new DateTime("now", $UTC_DateTimeZone);
        $timeOffset_seconds = $DateTimeZone->getOffset($UTC_datetime);
        //$timeOffset_hrs = ($timeOffset_seconds)/(60*60);
        //$timeOffset_hrs_disp = ($timeOffset_hrs>=0)?"+$timeOffset_hrs":$timeOffset_hrs;

        // extract hours
        $timeOffset_hrs = floor($timeOffset_seconds / (60 * 60));
        // extract minutes
        $divisor_for_minutes = $timeOffset_seconds % (60 * 60);
        $timeOffset_mins = floor($divisor_for_minutes / 60);
        $timeOffset_hrs = ($timeOffset_mins>0)?"$timeOffset_hrs.$timeOffset_mins":$timeOffset_hrs;
        $timeOffset_hrs_disp = ($timeOffset_hrs>=0)?"+$timeOffset_hrs":$timeOffset_hrs;

        if($type == 'hours')
            return $timeOffset_hrs_disp;
        else
            return $timeOffset_seconds;
    }

    /**
     * Encodes a string as base64, and sanitizes it for use in a CI URI.
     *
     * @param string $str The string to encode
     * @return string
     */
    function base64UrlEncode($str = '')
    {
        return strtr(base64_encode($str), '+=/', '.-~');
    }

    /**
     * Decodes a base64 string that was encoded by url_base64_encode.
     *
     * @param object $str The base64 string to decode.
     * @return string
     */
    function base64UrlDecode($str = '')
    {
        return base64_decode(strtr($str, '.-~', '+=/'));
    }

    function timePassed($timestamp)
    {
        $diff = time() - (int)$timestamp;

        if ($diff == 0)
            return 'just now';

        $intervals = array
        (
            1                   => array('year',    31556926),
            $diff < 31556926    => array('month',   2628000),
            $diff < 2629744     => array('week',    604800),
            $diff < 604800      => array('day',     86400),
            $diff < 86400       => array('hour',    3600),
            $diff < 3600        => array('minute',  60),
            $diff < 60          => array('second',  1)
        );

        $value = floor($diff/$intervals[1][1]);
        return $value.' '.$intervals[1][0].($value > 1 ? 's' : '').' ago';
    }

    function curlFileGetContents($f_data)
    {
        $curl = curl_init();
        $userAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';

        curl_setopt($curl,CURLOPT_URL,$f_data['url']);	//The URL to fetch. This can also be set when initializing a session with curl_init().
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,TRUE);	//TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
        curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,5);	//The number of seconds to wait while trying to connect.

        curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);	//The contents of the "User-Agent: " header to be used in a HTTP request.
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);	//To follow any "Location: " header that the server sends as part of the HTTP header.
        curl_setopt($curl, CURLOPT_AUTOREFERER, TRUE);	//To automatically set the Referer: field in requests where it follows a Location: redirect.
        //curl_setopt($curl, CURLOPT_TIMEOUT, 10);	//The maximum number of seconds to allow cURL functions to execute.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);	//To stop cURL from verifying the peer's certificate.

        $contents = curl_exec($curl);
        curl_close($curl);
        return $contents;
    }

    function getIp() {

        //Just get the headers if we can or else use the SERVER global
        if ( function_exists( 'apache_request_headers' ) ) {
            $headers = apache_request_headers();
        } else {
            $headers = $_SERVER;
        }
        //Get the forwarded IP if it exists
        if ( array_key_exists( 'X-Forwarded-For', $headers ) && filter_var( $headers['X-Forwarded-For'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) {
            $the_ip = $headers['X-Forwarded-For'];
        } elseif ( array_key_exists( 'HTTP_X_FORWARDED_FOR', $headers ) && filter_var( $headers['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 )
        ) {
            $the_ip = $headers['HTTP_X_FORWARDED_FOR'];
        } else {
            $the_ip = filter_var( $_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 );
        }
        return $the_ip;
    }

    /*
     * ajaxResponse
     * function that handles the ajaxResponse
     * @args  	$html - the html data that has to be passed on to the javascript
     * 				$message - message that will be displayed in the footer region
     * 				$data - optional data array, to be passed to the javascript
     * 				$result - optional variable to denote success or failure
     */
    function ajaxResponse($html, $message = '', $data=array(), $result = 1)
    {
        $CI =& get_instance();

        $requestURIString = (isset($_SERVER['REQUEST_URI']))?$_SERVER['REQUEST_URI']:'';
        if (strpos($requestURIString,'?')) {
            list($requestURIString,$params) = explode('?', $requestURIString);
        }

        $SCRIPT_NAME = (isset($_SERVER['SCRIPT_NAME']))?$_SERVER['SCRIPT_NAME']:'';
        $requestURI = explode('/', $requestURIString);
        $scriptName = explode('/',$SCRIPT_NAME);
        $commandArray = array_diff_assoc($requestURI,$scriptName);
        $commandArray = array_values($commandArray);
        $class = isset($commandArray[0])?$commandArray[0]:'user';
        $action = isset($commandArray[1])?$commandArray[1]:'index';
        $_POST['CALLBACK'] = isset($_POST['CALLBACK'])?$_POST['CALLBACK']:$class . '_' . $action;

        //$myResult['LOCK'] = $_POST['LOCK'];
        //$myResult['SHOWLOADING'] = isset($_POST['SHOWLOADING'])?$_POST['SHOWLOADING']:FALSE;
        $myResult['CB'] = $CI->input->post('CALLBACK');
        $myResult['HTML'] = ($html); //cleanText
//		$myResult['HTML'] = $html;
        $myResult['MESSAGE'] = ($message);
        $myResult['DATA'] = $data;
        $myResult['LOGGED_IN'] = $this->is_valid_session();
        $myResult['RESULT'] = $result;

        header("Cache-Control: no-cache, must-revalidate" );
        header("Pragma: no-cache" );
        //header("Content-type: application/json");
        // for jquery bug,
        header('Content-type: text/plain');
        print json_encode($myResult);
        return;
    }
}