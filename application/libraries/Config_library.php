<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// libraries/Mylib.php
class Config_library {
    var $ci;
    //var $key;

    function __construct() {

        $this->ci =& get_instance();
        //$this->ci->config->load('mylib');

        $is_valid_session = $this->ci->common->is_valid_session();

        if($is_valid_session) {
            define('_ALM_UID', $this->ci->session->userdata('user_id'));

            //
            /*$user_subscription = $this->ci->session->userdata('user_subscription');

            $this->ci->config->set_item('IS_FREE_TRIAL',0);
            if(strtolower($user_subscription['subName'])=='free trial')
            {
                $this->ci->config->set_item('IS_FREE_TRIAL',1);
            }*/
            //
        } else {
            define('_ALM_UID', 0);
        }

        //variable IS_AJAX will be true when the page request is an ajax type else false
        $ajax = 0;
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        {
            $ajax = 1;
        }
        elseif (isset($_POST['HTTP_X_REQUESTED_WITH']) && strtolower($_POST['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        {
            $ajax = 1;
        }
        define('IS_AJAX', $ajax);
    }
}

// config/mylib.php
//$config['key'] = 'randomcharacters';