<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Application_model extends CI_Model
{

    public $app_table_name;
    function Application_model()
    {
        parent::__construct();

        $this->load->library('common');
        $this->app_table_name='applications';
        $this->load->database();
    }

    public function install()
    {

    }

    /**
     * Get app info
     *
     * @param array $filt_data
     * @return array
     */
    public function getApplicationInfo($filt_data)
    {
        //
        $this->db->flush_cache();
        $select = (isset($filt_data['select']))?$filt_data['select']:'applications.*';
        $this->db->select($select);
        if(isset($filt_data['app_id']))
            $this->db->where('app_id', $filt_data['app_id']);
        if(isset($filt_data['fac_id']))
            $this->db->where('applications.fac_id', $filt_data['fac_id']);
        //if(isset($filt_data['site_id']))
            //$this->db->where_in('site_id', $filt_data['site_id']);
        if(isset($filt_data['get_fac_info']))
            $this->db->join('facilities', 'facilities.fac_id = applications.fac_id');
        //if(isset($filt_data['get_site_info']))
            //$this->db->join('sites', 'sites.site_id = applications.site_id');
        if(isset($filt_data['get_team_info'])) {
            $this->db->join('sur_teams', 'sur_teams.sur_team_id = applications.sur_team_id', 'left');
            if(isset($filt_data['team_lead'])) {
                $this->db->where('sur_teams.team_lead', $filt_data['team_lead']);
            }
            if(isset($filt_data['get_leader_info'])) {
                $this->db->join('users team_leader', 'sur_teams.team_lead = team_leader.user_id', 'left');
            }
            //if(isset($filt_data['is_acc_committee']))
                //$this->db->where('is_acc_committee', $filt_data['is_acc_committee']);
        }
        if(isset($filt_data['get_team_mem_info'])) {
            $this->db->join('sur_team_members', 'sur_team_members.sur_team_id = applications.sur_team_id', 'left');
            if(isset($filt_data['team_mem_id']))
                $this->db->where('sur_team_members.member_id', $filt_data['team_mem_id']);
        }
        if(isset($filt_data['get_c_team_info'])) {
            $this->db->join('sur_teams committee_teams', 'committee_teams.sur_team_id = applications.committee_team_id', 'left');
            if(isset($filt_data['c_team_lead'])) {
                $this->db->where('committee_teams.team_lead', $filt_data['c_team_lead']);
            }
            if(isset($filt_data['get_c_leader_info'])) {
                $this->db->join('users committee_leader', 'committee_teams.team_lead = committee_leader.user_id', 'left');
            }
            //if(isset($filt_data['is_acc_committee']))
            //$this->db->where('committee_teams.is_acc_committee', 1);
        }
        if(isset($filt_data['get_c_team_mem_info'])) {
            $this->db->join('sur_team_members committee_team_members', 'committee_team_members.sur_team_id = applications.committee_team_id', 'left');
            if(isset($filt_data['committee_team_mem_id']))
                $this->db->where('committee_team_members.member_id', $filt_data['committee_team_mem_id']);
        }
        if(isset($filt_data['get_owner_info']))
            $this->db->join('users', 'users.user_id = applications.created_by', 'left');
        if(isset($filt_data['min_app_status']))
            $this->db->where('app_status >=', $filt_data['min_app_status']);
        if(isset($filt_data['max_app_status']))
            $this->db->where('app_status <=', $filt_data['max_app_status']);
        $app_info_query = $this->db->get('applications');  //, $limit, $offset
        //echo $this->db->last_query();
        $app_info_res = $app_info_query->result_array();

        return $app_info_res;
    }

    /**
     * Get applications
     *
     * @param array $filt_data
     * @return array
     */
    public function getApplications($filt_data)
    {
        $rec_limit = (isset($filt_data['rec_limit']))?$filt_data['rec_limit']:'MAX_ROWS';
        $offset = (isset($filt_data['offset']))?$filt_data['offset']:0;;
        $select = (isset($filt_data['select']))?$filt_data['select']:'applications.*';

        $order_by = (isset($filt_data['order_by']))?$filt_data['order_by']:'app_type';
        $order_dir = (isset($filt_data['order_dir']))?$filt_data['order_dir']:'ASC';

        $this->db->flush_cache();
        $this->db->select("SQL_CALC_FOUND_ROWS null as tot_recs,$select", FALSE);
        if(isset($filt_data['get_fac_info'])) {
            $this->db->join('facilities', 'facilities.fac_id = applications.fac_id');
            if(isset($filt_data['fac_status'])) {
                $this->db->where('fac_status', $filt_data['fac_status']);
            }
        }
        if(isset($filt_data['q'])) {
            $this->db->group_start();
            $this->db->like('applications.app_id', $filt_data["q"], 'after');
            if(isset($filt_data['get_fac_info'])) {
                $this->db->or_like('facilities.name', $filt_data["q"]);
            }
            if(isset($filt_data['get_site_info'])) {
                $this->db->or_like('sites.site_names', $filt_data["q"]);
            }
            //$this->db->or_like('CONCAT(f_name," ",l_name)', $filt_data["q"]);
            $this->db->group_end();

            /*if(isset($filt_data['get_site_info'])) {
                $this->db->having('user_id', 45);
            }*/
        }
        if(isset($filt_data['get_site_info'])) {
            /*$this->db->join('app_sites', 'app_sites.app_id = applications.app_id');
            $this->db->join('sites', 'sites.site_id = app_sites.site_id');*/
            $this->db->join(' ( select GROUP_CONCAT(site_name) site_names,app_sites.app_id from app_sites
                                left join sites on sites.site_id = app_sites.site_id
                                group by app_sites.app_id
                              ) as sites', 'sites.app_id = applications.app_id');
        }
        if(isset($filt_data['get_owner_info']))
            $this->db->join('users', 'users.user_id = applications.created_by', 'left');
        if(isset($filt_data['get_team_info'])) {
            $this->db->join('sur_teams', 'sur_teams.sur_team_id = applications.sur_team_id', 'left');
            if(isset($filt_data['team_lead']))
                $this->db->where('sur_teams.team_lead', $filt_data['team_lead']);
            //if(isset($filt_data['is_acc_committee']))
                //$this->db->where('is_acc_committee', $filt_data['is_acc_committee']);
        }
        if(isset($filt_data['get_team_mem_info'])) {
            $this->db->join('sur_team_members', 'sur_team_members.sur_team_id = applications.sur_team_id', 'left');
            if(isset($filt_data['team_mem_id']))
                $this->db->where('member_id', $filt_data['team_mem_id']);
        }

        if(isset($filt_data['get_c_team_info'])) {
            $this->db->join('sur_teams committee_teams', 'committee_teams.sur_team_id = applications.committee_team_id', 'left');
            if(isset($filt_data['c_team_lead'])) {
                $this->db->where('committee_teams.team_lead', $filt_data['c_team_lead']);
            }
            if(isset($filt_data['get_c_leader_info'])) {
                $this->db->join('users committee_leader', 'committee_teams.team_lead = committee_leader.user_id', 'left');
            }
            //if(isset($filt_data['is_acc_committee']))
            //$this->db->where('committee_teams.is_acc_committee', 1);
        }
        if(isset($filt_data['get_c_team_mem_info'])) {
            $this->db->join('sur_team_members committee_team_members', 'committee_team_members.sur_team_id = applications.committee_team_id', 'left');
            if(isset($filt_data['committee_team_mem_id']))
                $this->db->where('committee_team_members.member_id', $filt_data['committee_team_mem_id']);
        }
        if(isset($filt_data['fac_id']))
            $this->db->where_in('applications.fac_id', $filt_data['fac_id']);
        //if(isset($filt_data['site_id']))
            //$this->db->where_in('applications.site_id', $filt_data['site_id']);
        if(isset($filt_data['exclude_app_id']))
            $this->db->where_not_in('applications.app_id', $filt_data['exclude_app_id']);
        if(isset($filt_data['sur_team_id']))
            $this->db->where('applications.sur_team_id', $filt_data['sur_team_id']);
        if(isset($filt_data['app_status']))
            $this->db->where('applications.app_status', $filt_data['app_status']);
        if(isset($filt_data['min_app_status']))
            $this->db->where('app_status >=', $filt_data['min_app_status']);
        if(isset($filt_data['max_app_status']))
            $this->db->where('app_status <=', $filt_data['max_app_status']);
        if(isset($filt_data['q'])) {
            $this->db->group_by('applications.app_id');
        }
        $this->db->order_by($order_by, $order_dir);
        $applications_query = $this->db->get('applications', $rec_limit, $offset);
        $result['apps'] = $applications_query->result_array();
        //echo $this->db->last_query();
        //print_r($result['apps']);
        //return;

        $count_query = $this->db->query('SELECT FOUND_ROWS() AS `count`');
        $result["tot_apps_count"] = $count_query->row()->count;
        return $result;
    }


    /**
     * Get app ids from applications
     *
     * @param array $applications
     * @return array
     */
    public function getApplicationIds($applications)
    {
        $app_ids = array();
        foreach($applications as $k => $v) {
            $app_ids[] = $v['app_id'];
        }

        return $app_ids;
    }

    /**
     * Get Sites linked with application
     *
     * @param array $filt_data
     * @return array
     */
    public function getAppSites($filt_data)
    {
        $rec_limit = (isset($filt_data['rec_limit']))?$filt_data['rec_limit']:1000;
        $offset = (isset($filt_data['offset']))?$filt_data['offset']:0;
        $select = (isset($filt_data['select']))?$filt_data['select']:'app_sites.*';

        $this->db->flush_cache();
        $this->db->select("SQL_CALC_FOUND_ROWS null as tot_recs,$select", FALSE);

        if(isset($filt_data['app_id']))
            $this->db->where_in('app_sites.app_id', $filt_data['app_id']);
        if(isset($filt_data['site_id']))
            $this->db->where_in('app_sites.site_id', $filt_data['site_id']);
        $this->db->join('sites', 'sites.site_id = app_sites.site_id');
        $app_sites_query = $this->db->get('app_sites', $rec_limit, $offset);
        $result['app_sites'] = $app_sites_query->result_array();
        //echo $this->db->last_query();
        //print_r($applications_res);
        //return;

        $count_query = $this->db->query('SELECT FOUND_ROWS() AS `count`');
        $result["tot_app_sites_count"] = $count_query->row()->count;
        return $result;
    }

    /**
     * Get SurTeamAnalysis info
     *
     * @param array $filt_data
     * @return array
     */
    public function getSurTeamAnalysis($filt_data)
    {
        //
        $this->db->flush_cache();
        $select = (isset($filt_data['select']))?$filt_data['select']:'sur_team_analysis.*';
        $this->db->select($select);
        if(isset($filt_data['app_id']))
            $this->db->where('app_id', $filt_data['app_id']);
        $ana_info_query = $this->db->get('sur_team_analysis');  //, $limit, $offset
        //echo $this->db->last_query();
        $ana_info_res = $ana_info_query->result_array();

        return $ana_info_res;
    }

    /**
     * Get status history with application
     *
     * @param array $filt_data
     * @return array
     */
    public function getStatusHistory($filt_data)
    {
        $rec_limit = (isset($filt_data['rec_limit']))?$filt_data['rec_limit']:100000;
        $offset = (isset($filt_data['offset']))?$filt_data['offset']:0;
        $select = (isset($filt_data['select']))?$filt_data['select']:'app_status_history.*';

        $this->db->flush_cache();
        $this->db->select("SQL_CALC_FOUND_ROWS null as tot_recs,$select", FALSE);
        //$this->db->join('app_standards', 'app_standards.app_std_id = app_recommendations.app_std_id', 'left');
        $this->db->join('roles', 'app_status_history.role_id = roles.role_id', 'left');
        if(isset($filt_data['get_owner_info']))
            $this->db->join('users', 'users.user_id = app_status_history.user_id', 'left');
        if(isset($filt_data['s_history_id']))
            $this->db->where_in('app_status_history.s_history_id', $filt_data['s_history_id']);
        if(isset($filt_data['app_id']))
            $this->db->where_in('app_status_history.app_id', $filt_data['app_id']);
        if(isset($filt_data['user_id']))
            $this->db->where('app_status_history.user_id', $filt_data['user_id']);
        $rec_query = $this->db->get('app_status_history', $rec_limit, $offset);
        $result['s_history'] = $rec_query->result_array();
        //echo $this->db->last_query();
        //print_r($applications_res);
        //return;

        $count_query = $this->db->query('SELECT FOUND_ROWS() AS `count`');
        $result["tot_s_history_count"] = $count_query->row()->count;
        return $result;
    }

    /**
     * Get recommendations linked with application
     *
     * @param array $filt_data
     * @return array
     */
    public function getRecommendations($filt_data)
    {
        $rec_limit = (isset($filt_data['rec_limit']))?$filt_data['rec_limit']:100000;
        $offset = (isset($filt_data['offset']))?$filt_data['offset']:0;
        $select = (isset($filt_data['select']))?$filt_data['select']:'app_recommendations.*';

        $this->db->flush_cache();
        $this->db->select("SQL_CALC_FOUND_ROWS null as tot_recs,$select", FALSE);
        $this->db->join('app_standards', 'app_standards.app_std_id = app_recommendations.app_std_id', 'left');
        $this->db->join('standards', 'standards.standard_id = app_standards.standard_id', 'left');
        if(isset($filt_data['is_remind_email_sent']))
            $this->db->where('app_recommendations.is_remind_email_sent', $filt_data['is_remind_email_sent']);
        if(isset($filt_data['is_due_email_sent']))
            $this->db->where('app_recommendations.is_due_email_sent', $filt_data['is_due_email_sent']);
        if(isset($filt_data['get_owner_info']))
            $this->db->join('users', 'users.user_id = app_recommendations.user_id', 'left');
        if(isset($filt_data['rec_id']))
            $this->db->where_in('app_recommendations.rec_id', $filt_data['rec_id']);
        if(isset($filt_data['app_id']))
            $this->db->where_in('app_standards.app_id', $filt_data['app_id']);
        if(isset($filt_data['app_std_id']))
            $this->db->where_in('app_recommendations.app_std_id', $filt_data['app_std_id']);
        if(isset($filt_data['user_id']))
            $this->db->where('app_recommendations.user_id', $filt_data['user_id']);
        if(isset($filt_data['rec_status']))
            $this->db->where('app_recommendations.rec_status', $filt_data['rec_status']);
        if(isset($filt_data['max_remind_utc'])) {
            $this->db->where('app_recommendations.remind_utc > 0');
            $this->db->where('app_recommendations.remind_utc <= '.$filt_data['max_remind_utc']);
        }
        if(isset($filt_data['max_due_by_utc'])) {
            $this->db->where('app_recommendations.due_by_utc > 0');
            $this->db->where('app_recommendations.due_by_utc <= '.$filt_data['max_due_by_utc']);
        }
        $rec_query = $this->db->get('app_recommendations', $rec_limit, $offset);
        $result['recommendations'] = $rec_query->result_array();
        //echo $this->db->last_query();
        //print_r($applications_res);
        //return;

        $count_query = $this->db->query('SELECT FOUND_ROWS() AS `count`');
        $result["tot_rec_count"] = $count_query->row()->count;
        return $result;
    }

    /**
     * Get recommendation info
     *
     * @param array $filt_data
     * @return array
     */
    public function getRecommendationInfo($filt_data)
    {
        //
        $this->db->flush_cache();
        $select = (isset($filt_data['select']))?$filt_data['select']:'app_recommendations.*';
        $this->db->select($select);
        if(isset($filt_data['rec_id']))
            $this->db->where('rec_id', $filt_data['rec_id']);

        $rec_info_query = $this->db->get('app_recommendations');  //, $limit, $offset
        $rec_info_res = $rec_info_query->result_array();

        return $rec_info_res;
    }

    /**
     * Get actions linked with application
     *
     * @param array $filt_data
     * @return array
     */
    public function getActions($filt_data)
    {
        $rec_limit = (isset($filt_data['rec_limit']))?$filt_data['rec_limit']:100000;
        $offset = (isset($filt_data['offset']))?$filt_data['offset']:0;
        $select = (isset($filt_data['select']))?$filt_data['select']:'app_actions.*';

        $this->db->flush_cache();
        $this->db->select("SQL_CALC_FOUND_ROWS null as tot_recs,$select", FALSE);
        if(isset($filt_data['is_remind_email_sent']))
            $this->db->where('app_actions.is_remind_email_sent', $filt_data['is_remind_email_sent']);
        if(isset($filt_data['is_due_email_sent']))
            $this->db->where('app_actions.is_due_email_sent', $filt_data['is_due_email_sent']);
        if(isset($filt_data['get_owner_info']))
            $this->db->join('users', 'users.user_id = app_actions.user_id', 'left');
        if(isset($filt_data['action']))
            $this->db->where_in('app_actions.action', $filt_data['action']);
        if(isset($filt_data['app_id']))
            $this->db->where_in('app_actions.app_id', $filt_data['app_id']);
        if(isset($filt_data['user_id']))
            $this->db->where('app_actions.user_id', $filt_data['user_id']);
        if(isset($filt_data['action_status']))
            $this->db->where('app_actions.action_status', $filt_data['action_status']);
        if(isset($filt_data['max_remind_utc'])) {
            $this->db->where('app_actions.remind_utc > 0');
            $this->db->where('app_actions.remind_utc <= '.$filt_data['max_remind_utc']);
        }
        if(isset($filt_data['max_due_by_utc'])) {
            $this->db->where('app_actions.due_by_utc > 0');
            $this->db->where('app_actions.due_by_utc <= '.$filt_data['max_due_by_utc']);
        }
        $rec_query = $this->db->get('app_actions', $rec_limit, $offset);
        $result['actions'] = $rec_query->result_array();
        //echo $this->db->last_query();
        //print_r($applications_res);
        //return;

        $count_query = $this->db->query('SELECT FOUND_ROWS() AS `count`');
        $result["tot_action_count"] = $count_query->row()->count;
        return $result;
    }

    /**
     * Get action info
     *
     * @param array $filt_data
     * @return array
     */
    public function getActionInfo($filt_data)
    {
        //
        $this->db->flush_cache();
        $select = (isset($filt_data['select']))?$filt_data['select']:'app_actions.*';
        $this->db->select($select);
        if(isset($filt_data['action_id']))
            $this->db->where('action_id', $filt_data['action_id']);

        $app_info_query = $this->db->get('app_actions');  //, $limit, $offset
        $app_info_res = $app_info_query->result_array();

        return $app_info_res;
    }

    /**
     * Get tasks linked with application
     *
     * @param array $filt_data
     * @return array
     */
    public function getTasks($filt_data)
    {
        $rec_limit = (isset($filt_data['rec_limit']))?$filt_data['rec_limit']:100000;
        $offset = (isset($filt_data['offset']))?$filt_data['offset']:0;
        $select = (isset($filt_data['select']))?$filt_data['select']:'tasks.*';

        $this->db->flush_cache();
        $this->db->select("SQL_CALC_FOUND_ROWS null as tot_recs,$select", FALSE);
        if(isset($filt_data['get_owner_info']))
            $this->db->join('users', 'users.user_id = tasks.user_id', 'left');
        if(isset($filt_data['task_id']))
            $this->db->where_in('tasks.task_id', $filt_data['task_id']);
        if(isset($filt_data['app_std_id']))
            $this->db->where_in('tasks.app_std_id', $filt_data['app_std_id']);
        if(isset($filt_data['t_status']))
            $this->db->where('tasks.t_status', $filt_data['t_status']);
        if(isset($filt_data['is_email_sent']))
            $this->db->where('tasks.is_email_sent', $filt_data['is_email_sent']);
        if(isset($filt_data['user_id']))
            $this->db->where('tasks.user_id', $filt_data['user_id']);
        if(isset($filt_data['max_remind_utc']))
            $this->db->where('tasks.remind_utc <= '.$filt_data['max_remind_utc']);
        $tasks_query = $this->db->get('tasks', $rec_limit, $offset);
        $result['tasks'] = $tasks_query->result_array();
        //echo $this->db->last_query();
        //print_r($applications_res);
        //return;

        $count_query = $this->db->query('SELECT FOUND_ROWS() AS `count`');
        $result["tot_tasks_count"] = $count_query->row()->count;
        return $result;
    }

    /**
     * Get task info
     *
     * @param array $filt_data
     * @return array
     */
    public function getTaskInfo($filt_data)
    {
        //
        $this->db->flush_cache();
        $select = (isset($filt_data['select']))?$filt_data['select']:'tasks.*';
        $this->db->select($select);
        if(isset($filt_data['task_id']))
            $this->db->where('task_id', $filt_data['task_id']);

        $task_info_query = $this->db->get('tasks');  //, $limit, $offset
        $task_info_res = $task_info_query->result_array();

        return $task_info_res;
    }

    /**
     * Get staff info
     *
     * @param array $filt_data
     * @return array
     */
    public function getStaffInfo($filt_data)
    {
        //
        $this->db->flush_cache();
        $select = (isset($filt_data['select']))?$filt_data['select']:'app_std_staff.*';
        $this->db->select($select);
        if(isset($filt_data['get_user_info'])) {
            $this->db->join('users', 'users.user_id = app_std_staff.user_id', 'left');
            $this->db->join('user_profile', 'user_profile.user_id = users.user_id', 'left');
        }
        /*if(isset($filt_data['staff_id']))
            $this->db->where('staff_id', $filt_data['staff_id']);*/
        if(isset($filt_data['app_std_id']))
            $this->db->where_in('app_std_staff.app_std_id', $filt_data['app_std_id']);
        if(isset($filt_data['user_id']))
            $this->db->where('app_std_staff.user_id', $filt_data['user_id']);
        $staff_info_query = $this->db->get('app_std_staff');  //, $limit, $offset
        $staff_info_res = $staff_info_query->result_array();

        return $staff_info_res;
    }

    /**
     * Get staff linked with application
     *
     * @param array $filt_data
     * @return array
     */
    public function getStaff($filt_data)
    {
        $rec_limit = (isset($filt_data['rec_limit']))?$filt_data['rec_limit']:100000;
        $offset = (isset($filt_data['offset']))?$filt_data['offset']:0;
        $select = (isset($filt_data['select']))?$filt_data['select']:'app_std_staff.*';

        $this->db->flush_cache();
        $this->db->select("SQL_CALC_FOUND_ROWS null as tot_recs,$select", FALSE);
        if(isset($filt_data['get_user_info'])) {
            $this->db->join('users', 'users.user_id = app_std_staff.user_id', 'left');
            $this->db->join('user_profile', 'user_profile.user_id = users.user_id', 'left');
        }
        if(isset($filt_data['std_staff_id']))
            $this->db->where_in('app_std_staff.std_staff_id', $filt_data['std_staff_id']);
        if(isset($filt_data['app_std_id']))
            $this->db->where_in('app_std_staff.app_std_id', $filt_data['app_std_id']);
        if(isset($filt_data['app_id']))
            $this->db->where_in('app_std_staff.app_id', $filt_data['app_id']);
        if(isset($filt_data['user_id']))
            $this->db->where('app_std_staff.user_id', $filt_data['user_id']);
        $staff_query = $this->db->get('app_std_staff', $rec_limit, $offset);
        $result['staff'] = $staff_query->result_array();
        //echo $this->db->last_query();
        //print_r($applications_res);
        //return;

        $count_query = $this->db->query('SELECT FOUND_ROWS() AS `count`');
        $result["tot_staff_count"] = $count_query->row()->count;
        return $result;
    }

    /**
     * Get staff Positions
     *
     * @param array $filt_data
     * @return array
     */
    public function getStaffPositions($filt_data)
    {
        $rec_limit = (isset($filt_data['rec_limit']))?$filt_data['rec_limit']:100000;
        $offset = (isset($filt_data['offset']))?$filt_data['offset']:0;
        $select = (isset($filt_data['select']))?$filt_data['select']:'user_positions.*';

        $this->db->flush_cache();
        $this->db->select("SQL_CALC_FOUND_ROWS null as tot_recs,$select", FALSE);
        if(isset($filt_data['user_id']))
            $this->db->where_in('user_positions.user_id', $filt_data['user_id']);
        if(isset($filt_data['pos_id']))
            $this->db->where('user_positions.pos_id', $filt_data['pos_id']);
        $pos_query = $this->db->get('user_positions', $rec_limit, $offset);
        $result['staff_pos'] = $pos_query->result_array();
        //echo $this->db->last_query();
        //print_r($applications_res);
        //return;

        $count_query = $this->db->query('SELECT FOUND_ROWS() AS `count`');
        $result["tot_staff_pos_count"] = $count_query->row()->count;
        return $result;
    }

    /**
     * Get files
     *
     * @param array $filt_data
     * @return array
     */
    public function getAppFiles($filt_data)
    {
        $rec_limit = (isset($filt_data['rec_limit']))?$filt_data['rec_limit']:100000;
        $offset = (isset($filt_data['offset']))?$filt_data['offset']:0;
        $select = (isset($filt_data['select']))?$filt_data['select']:'app_files.*';

        $this->db->flush_cache();
        $this->db->select("SQL_CALC_FOUND_ROWS null as tot_recs,$select", FALSE);
        if(isset($filt_data['get_owner_info']))
            $this->db->join('users', 'users.user_id = app_files.user_id', 'left');
        if(isset($filt_data['app_file_id']))
            $this->db->where_in('app_files.app_file_id', $filt_data['app_file_id']);
        if(isset($filt_data['app_std_id']))
            $this->db->where_in('app_files.app_std_id', $filt_data['app_std_id']);
        if(isset($filt_data['site_id']))
            $this->db->where('app_files.site_id', $filt_data['site_id']);
        if(isset($filt_data['fac_id']))
            $this->db->where('app_files.fac_id', $filt_data['fac_id']);
        if(isset($filt_data['user_id']))
            $this->db->where('app_files.user_id', $filt_data['user_id']);
        $app_files_query = $this->db->get('app_files', $rec_limit, $offset);
        $result['app_files'] = $app_files_query->result_array();
        //echo $this->db->last_query();
        //print_r($applications_res);
        //return;

        $count_query = $this->db->query('SELECT FOUND_ROWS() AS `count`');
        $result["tot_app_files_count"] = $count_query->row()->count;
        return $result;
    }

    public function clear_db ()
    {
        $this->db->query("DELETE FROM `applications`");
    }
}
