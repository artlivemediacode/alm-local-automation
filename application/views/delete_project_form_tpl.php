<form class="form-horizontal form-bordereds pa15 pt0 panel panel-primary" action="/install/delete_project" name="frm_delete_project" id="frm_delete_project">

    <div class="panel-body pt0 mt15">
<!--            <p>Fill in your project details</p>-->
        <div class="form-group frm_alert_hld"></div><!-- will be used as done/fail message container -->
        <div class="form-group">
            <div class="row">
                <div class="col-sm-6">
                    <label class="control-label mb5 pt0">Project Name</label>
                    <input type="text" class="form-control project_name" name="project_name" placeholder="">
                    <span class="help-block">Lowercase, no spaces, no special characters.(Eg. hello_world)</span>
                </div>
                <div class="col-sm-6">
                    <label class="control-label mb5 pt0">Project Path</label>
                    <input type="text" class="form-control project_path" name="project_path" placeholder="">
                    <span class="help-block">Absolute path to where you want your project folder created.(Eg. F:/Projects)</span>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-sm-6">
                    <label class="control-label mb5 pt0">Localhost</label>
                    <input type="text" class="form-control localhost" name="localhost" placeholder="">
                    <span class="help-block">Lowercase, no spaces, no special characters.(Eg. loc.hello_world.com)</span>
                </div>
                <div class="col-sm-6">
                    <label class="control-label mb5 pt0">VHOSTS file</label>
                    <input type="text" class="form-control vhosts" name="vhosts" value="<?php echo $vhosts_path; ?>" _value="C:\Bitnami\wampstack-5.5.26-0\apache2\conf\extra\httpd-vhosts.conf">
                    <!--                        <span class="help-block">Lowercase, no spaces, no special characters.(Eg. loc.hello_world.com)</span>-->
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-sm-6">
                    <label class="control-label mb5 pt0">Database</label>
                    <input type="text" class="form-control database" name="database" placeholder="">
                    <span class="help-block text-danger">Please double check your database name as this process is not reversible.</span>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <button type="submit" class="btn btn-danger btn-submit ladda-button pull-right" id="" data-style="expand-right">
            <span class="ladda-label">DELETE PROJECT!</span>
        </button>
    </div>
</form>
