<footer data-stripe_pub="<?php //echo config_item('stripe_pub_key'); ?>">


</footer>

<!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=true"></script>-->
<!--<script type="text/javascript" src="/assets/library/jquery/js/jquery.min.js"></script>-->
<!--<script type="text/javascript" src="/assets/plugins/jqueryui/js/jquery-ui.js"></script>-->
<!--<script>-->
<!--    /*** Handle jQuery plugin naming conflict between jQuery UI and Bootstrap ***/-->
<!--    $.widget.bridge('uibutton', $.ui.button);-->
<!--    $.widget.bridge('uitooltip', $.ui.tooltip);-->
<!--</script>-->

<!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
<!-- Library script : mandatory -->
<!--<script type="text/javascript" src="/assets/library/jquery/js/jquery.min.js"></script>-->
<!--<script type="text/javascript" src="/assets/library/jquery/js/jquery-migrate.min.js"></script>-->
<!--<script type="text/javascript" src="/assets/library/bootstrap/js/bootstrap.min.js"></script>-->
<script type="text/javascript" src="/assets/javascript/vendor.js"></script>
<script type="text/javascript" src="/assets/library/core/js/core.js"></script>
<script type="text/javascript" src="/assets/javascript/app.js"></script>
<!--/ Library script -->

<script>
    //Need this to overide jquery UI's tooltip method //todo raja move this ugly line to a separate file
    $.fn.bootstrapTip = $.fn.tooltip.noConflict();
</script>

<!-- App and page level script -->
<!--<script type="text/javascript" src="/assets/plugins/sparkline/js/jquery.sparkline.min.js"></script>-->

<script type="text/javascript" src="/assets/plugins/selectize/js/selectize.js"></script>
<script type="text/javascript" src="/assets/plugins/jqueryui/js/jquery-ui.js"></script>
<script type="text/javascript" src="/assets/plugins/jqueryform/jquery.form.js"></script>
<script type="text/javascript" src="/assets/plugins/jqueryui/js/jquery-ui-timepicker.js"></script>

<!--<script src="/assets/javascript/bootstrap3-dialog-master/src/js/bootstrap-dialog.js"></script>-->

<script src="/assets/plugins/bootbox/js/bootbox.min.js"></script>

<script src="/assets/javascript/moment.min.js"></script>
<!--<script src="/assets/javascript/livestamp.js"></script>-->
<script src="/assets/javascript/jquery.timeago.js"></script>
<script type="text/javascript" src="/assets/plugins/summernote/js/summernote.min.js"></script>

<script type="text/javascript" src="/assets/javascript/JavaScript-Load-Image-master/js/load-image.all.min.js"></script>
<script type="text/javascript" src="/assets/javascript/jQuery-File-Upload-9.9.3/js/jquery.fileupload.js"></script>
<script type="text/javascript" src="/assets/javascript/jQuery-File-Upload-9.9.3/js/jquery.fileupload-ui.js"></script>
<script type="text/javascript" src="/assets/javascript/jQuery-File-Upload-9.9.3/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="/assets/javascript/jQuery-File-Upload-9.9.3/js/jquery.fileupload-process.js"></script>
<script type="text/javascript" src="/assets/javascript/jQuery-File-Upload-9.9.3/js/jquery.fileupload-image.js"></script>
<script type="text/javascript" src="/assets/javascript/jQuery-File-Upload-9.9.3/js/jquery.fileupload-validate.js"></script>

<script type="text/javascript" src="/assets/javascript/php_js.js"></script>
<script type="text/javascript" src="/assets/javascript/main.js"></script>
<script type="text/javascript" src="/assets/javascript/common.lib.js"></script>
<script type="text/javascript" src="/assets/javascript/common_validation.js"></script>

<?php if(isset($dashboard_page)):?>
    <!--    <script type="text/javascript" src="/assets/javascript/pages/dashboard-v2.js"></script>-->
<?php endif;?>

<!--<script type="text/javascript" src="/assets/javascript/roles.js"></script>-->
<!--<script type="text/javascript" src="/assets/javascript/permissions.js"></script>-->
<!--<script type="text/javascript" src="/assets/javascript/facility.js"></script>-->
<!--<script type="text/javascript" src="/assets/javascript/sites.js"></script>-->
<!--<script type="text/javascript" src="/assets/javascript/surveyor_teams.js"></script>-->
<!--<script type="text/javascript" src="/assets/javascript/users.js"></script>-->
<!--<script type="text/javascript" src="/assets/javascript/category.js"></script>-->
<!--<script type="text/javascript" src="/assets/javascript/profile.js"></script>-->
<!--<script type="text/javascript" src="/assets/javascript/term_categories.js"></script>-->
<!--<script type="text/javascript" src="/assets/javascript/applications.js"></script>-->
<!--<script type="text/javascript" src="/assets/javascript/questions.js"></script>-->
<!--<script type="text/javascript" src="/assets/javascript/help_topics.js"></script>-->
<!--<script type="text/javascript" src="/assets/javascript/video_help.js"></script>-->

<script type="text/javascript" src="/assets/javascript/install.js"></script>

<!--<script type="text/javascript" src="/assets/javascript/forms/element.js"></script>-->


<script type="text/javascript" src="/assets/javascript/feedback/stickyfloat.js"></script>
<script type="text/javascript" src="/assets/javascript/feedback/feedback.js"></script>


<!--<script type="text/javascript" src="/assets/javascript/tables_sort/jquery.tablesorter.js"></script>-->
<!--<script type="text/javascript" src="/assets/javascript/table_sort.js"></script>-->

<!--/ App and page level script -->
<!--/ END JAVASCRIPT SECTION -->
</body>
<!--/ END Body -->
</html>