<!DOCTYPE html>
<html class="backend">
<!-- START Head -->
<head>
    <!-- START META SECTION -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Artlivemedia</title>
    <meta property="og:title" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/assets/image/touch/apple-touch-icon-144x144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/assets/image/touch/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/assets/image/touch/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/image/touch/apple-touch-icon-57x57-precomposed.png">
<!--    <link rel="shortcut icon" href="/assets/image/favicon.ico">-->
    <!--/ END META SECTION -->

    <!-- START STYLESHEETS -->
    <!-- Plugins stylesheet : optional -->
    <link rel="stylesheet" href="/assets/plugins/selectize/css/selectize.min.css">
    <link rel="stylesheet" href="/assets/plugins/jqueryui/css/jquery-ui.min.css">
    <link rel="stylesheet" href="/assets/plugins/jqueryui/css/jquery-ui-timepicker.min.css">

    <link rel="stylesheet" href="/assets/plugins/owl/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/assets/plugins/datatables/css/jquery.datatables.min.css">
<!--    <link rel="stylesheet" href="/assets/plugins/jvectormap/css/jvectormap.css">-->

<!--    <link rel="stylesheet" href="/assets/plugins/steps/css/jquery-steps.min.css">-->
    <link rel="stylesheet" href="/assets/plugins/fileupload/css/fileupload.css">

<!--    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="/assets/stylesheet/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/plugins/summernote/css/summernote.min.css">
    <!--/ Plugins stylesheet -->

    <!-- Application stylesheet : mandatory -->
<!--    <link rel="stylesheet" href="/assets/library/bootstrap/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="/assets/stylesheet/bootstrap.css">
    <link rel="stylesheet" href="/assets/stylesheet/layout.css">
    <link rel="stylesheet" href="/assets/stylesheet/uielement.css">
    <!--/ Application stylesheet -->
    <!-- END STYLESHEETS -->

    <!-- START JAVASCRIPT SECTION - Load only modernizr script here -->
    <script src="/assets/library/modernizr/js/modernizr.min.js"></script>
    <!--/ END JAVASCRIPT SECTION -->

<!--    <link rel="stylesheet" href="/assets/stylesheet/rony-style.css">-->
    <link rel="stylesheet" href="/assets/stylesheet/custom.css">

<!--    <link rel="stylesheet" href="/assets/javascript/bootstrap3-dialog-master/src/css/bootstrap_ui_alert.css">-->
</head>
<!--/ END Head -->

<!-- START Body -->
<body>
<?php //$this->load->view('g_analytics'); ?>
<!-- START Template Header -->
<header id="header" class="navbar navbar-fixed-top">
<!-- START navbar header -->
<div class="navbar-header bgcolor-white">
    <!-- Brand -->
    <a class="navbar-brand" href="/" style="
    font-size: 35px;
    font-weight: bold;
">
<!--        <h4 class="mt15 text-white">Bambi</h4>-->
<!--        <span class="logo-figure"></span>-->
<!--        <span class="functionmate-logo"></span>-->
        <img class="img" src="/assets/image/logo/app-logo.jpg" alt="" width="" height="60">
<!--        <img class="img" src="/assets/image/logo/logo-text.png" alt="" width="240" height="60">-->
    </a>
    <!--/ Brand -->
</div>
<!--/ END navbar header -->

<!-- START Toolbar -->
<div class="navbar-toolbar clearfix">
    <!-- START Left nav -->
    <ul class="nav navbar-nav navbar-left">
        <!-- Sidebar shrink -->
        <li class="hidden-xs hidden-sm">
            <a href="javascript:void(0);" class="sidebar-minimize" data-toggle="minimize" title="Minimize sidebar">
                <span class="meta">
                    <span class="icon"></span>
                </span>
            </a>
        </li>
        <!--/ Sidebar shrink -->

        <!-- Offcanvas left: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
        <li class="navbar-main hidden-lg hidden-md hidden-sm">
            <a href="javascript:void(0);" data-toggle="sidebar" data-direction="ltr" rel="tooltip" title="Menu sidebar">
                <span class="meta">
                    <span class="icon"><i class="ico-paragraph-justify3"></i></span>
                </span>
            </a>
        </li>
        <!--/ Offcanvas left -->

    </ul>
    <!--/ END Left nav -->
</div>
<!--/ END Toolbar -->
</header>
<!--/ END Template Header -->