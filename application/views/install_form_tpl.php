<form class="form-horizontal form-bordereds pa15 pt0 panel panel-primary" action="/install/run" name="frm_install" id="frm_install">

    <div class="panel-body pt0 mt15">
<!--            <p>Fill in your project details</p>-->
        <div class="form-group frm_alert_hld"></div><!-- will be used as done/fail message container -->
        <div class="form-group">
            <div class="row">
                    <div class="col-sm-6">
                        <label class="control-label mb5 pt0">Project Name</label>
                        <input type="text" class="form-control project_name" name="project_name" placeholder="">
                        <span class="help-block">Lowercase, no spaces, no special characters.(Eg. hello_world)</span>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label mb5 pt0">Project Path</label>
                        <div class="input-group input-group-sm--">
                            <input type="text" class="form-control project_path" name="project_path" placeholder="">
                            <span class="input-group-addon proj_name_clone"></span>
                        </div>
                        <span class="help-block">Absolute path to where you want your project folder created.(Eg. F:/Projects)</span>
                    </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-sm-6">
                    <label class="control-label mb5 pt0">Localhost</label>
                    <input type="text" class="form-control localhost" name="localhost" placeholder="">
                    <span class="help-block">Lowercase, no spaces, no special characters.(Eg. loc.hello_world.com)</span>
                </div>
                <div class="col-sm-6">
                    <label class="control-label mb5 pt0">VHOSTS file</label>
                    <input type="text" class="form-control vhosts" name="vhosts" value="<?php echo $vhosts_path; ?>" _value="C:\Bitnami\wampstack-5.5.26-0\apache2\conf\extra\httpd-vhosts.conf">
<!--                        <span class="help-block">Lowercase, no spaces, no special characters.(Eg. loc.hello_world.com)</span>-->
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-sm-4">
                    <label class="control-label mb5 pt0">Build</label>
                    <span class="checkbox custom-radio custom-radio-primary">
                        <input type="radio" class="build" name="build" id="wordpress" value="wordpress">
                        <label for="wordpress">&nbsp;&nbsp;Wordpress</label>
                    </span>
                    <span class="checkbox custom-radio custom-radio-primarye">
                        <input type="radio" class="build" name="build" id="wordpress_use_alm_theme" checked value="wordpress_use_alm_theme">
                        <label for="wordpress_use_alm_theme">&nbsp;&nbsp;Wordpress and use default theme.</label>
                    </span>
                    <span class="checkbox custom-radio custom-radio-primary">
                        <input type="radio" class="build" name="build" id="custom" value="custom">
                        <label for="custom">&nbsp;&nbsp;Custom.</label>
                    </span>
                </div>
<!--                    <div class="col-sm-4 hosting_hld hide">-->
<!--                        <label class="control-label mb5 pt0">Hosting</label>-->
<!--                        <span class="checkbox custom-radio custom-radio-primary">-->
<!--                            <input type="radio" class="hosting" name="hosting" id="wpengine" value="wpengine">-->
<!--                            <label for="wpengine">&nbsp;&nbsp;WPEngine</label>-->
<!--                        </span>-->
<!--                        <span class="checkbox custom-radio custom-radio-primary custom_hosting">-->
<!--                            <input type="radio" class="hosting" name="hosting" id="custom_hosting" checked value="custom_hosting">-->
<!--                            <label for="custom_hosting">&nbsp;&nbsp;Custom</label>-->
<!--                        </span>-->
<!--                    </div>-->
                <div class="col-sm-4">
                    <label class="control-label mb5 pt0">Git</label>
                    <span class="checkbox custom-radio custom-radio-primary">
                        <input type="radio" class="is_git" name="is_git" id="git" checked value="yes">
                        <label for="git">&nbsp;&nbsp;Yes</label>
                    </span>
                    <span class="checkbox custom-radio custom-radio-primary">
                        <input type="radio" class="is_git" name="is_git" id="no_git" value="no">
                        <label for="no_git">&nbsp;&nbsp;No</label>
                    </span>
                </div>
            </div>
        </div>

        <div class="form-group wpengine_hld">
            <div class="row">
                <div class="col-sm-4">
                    <label class="control-label mb5 pt0">WPEngine Hosting</label>
                    <span class="checkbox custom-checkbox custom-checkbox-primary">
                        <input type="checkbox" class="is_wpengine" name="is_wpengine" id="is_wpengine" value="yes">
                        <label for="is_wpengine">&nbsp;&nbsp;Is WPEngine</label>
                    </span>
                </div>
            </div>
        </div>

        <div class="form-group ">
            <div class="row ">
                <div class="col-sm-6 email_hld hide">
                    <label class="control-label mb5 pt0">Email</label>
                    <input type="email" class="form-control email" name="email" placeholder="">
                    <span class="help-block">Enter your email for ssh key generation</span>
                </div>

                <div class="col-sm-6 user_dir_hld hide">
                    <label class="control-label mb5 pt0">User Directory</label>
                    <select class="selectbox selectize user_directory" name="user_directory" style="display: inline-block">
                        <option value="" selected>Choose your user directory.</option>
                        <?php if(isset($user_dirs)): ?>
                            <?php foreach($user_dirs as $k => $v): ?>
                                <option value="<?php echo $v; ?>"><?php echo $v; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <span class="help-block">Creates USER_DIRECTORY/.ssh/config file under this directory if not exists.</span>
                </div>
            </div>
        </div>



<!--        <div class="form-group">-->
<!--            <div class="row">-->
<!--                <div class="col-sm-6">-->
<!--                    <label class="control-label mb5 pt0">DB User Name</label>-->
<!--                    <input type="text" class="form-control db_user_name" name="db_user_name" placeholder="">-->
<!--                </div>-->
<!--                <div class="col-sm-6">-->
<!--                    <label class="control-label mb5 pt0">DB Password</label>-->
<!--                    <input type="text" class="form-control db_pwd" name="db_pwd" placeholder="">-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
    </div>
    <div class="panel-footer">
        <button type="submit" class="btn btn-success btn-submit ladda-button pull-right" id="" data-style="expand-right">
            <span class="ladda-label">RUN!</span>
        </button>
    </div>
</form>
