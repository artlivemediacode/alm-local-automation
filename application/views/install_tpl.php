<?php echo $header; ?>

<?php //echo $left_sidebar; ?>

<!-- START Template Main -->
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <div class="col-sm-10">
                    <h4 class="title semibold">Artlivemedia Local Automation Script</h4>
                </div>
            </div>
        </div>
        <!-- Page Header -->

        <!-- START row -->
        <div class="row">

            <div class="col-md-12">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#add_project" data-toggle="tab">Add Project</a></li>
                    <li><a href="#remove_project" data-toggle="tab">Delete Project</a></li>
                </ul>
                <!-- START panel -->
                <div class="tab-content panel">
                    <div class="tab-pane active np" id="add_project">
                        <?php echo $install_form;?>
                    </div>
                    <div class="tab-pane np" id="remove_project">
                        <?php echo $delete_project_form;?>
                    </div>
                </div>





<!--                </div>-->
            </div>
        </div>
        <!--/ END row -->


    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->

</section>
<!--/ END Template Main -->
<?php echo $footer; ?>

