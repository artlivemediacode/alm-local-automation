
<!-- START Template Footer -->
<footer id="" role="contentinfo" class="bgcolor-dark pt25">
    <!-- container -->
    <div class="container mb25">
        <!-- row -->
        <div class="row">
            <!-- About -->
            <div class="col-md-4">
                <h4 class="font-alt mt0">About Us</h4>
                <p>Queensland Prevocational Medical Accreditation is committed to best practice accreditation of training for new doctors through the health system.</p>
                <p></p>
<!--                <a href="javascript:void(0);" class="text-primary">Learn More</a>-->
            </div>
            <!--/ About -->

            <!-- Address + Social -->
            <div class="col-md-4">
                <h4 class="font-alt mt0">Address</h4>
                <address>
                    Unit 1A, 32 Billabong Street<br>
                    Stafford, QLD 4053<br>
<!--                    <abbr title="Phone">P:</abbr> (123) 456-7890-->
                </address>
                <?php /*
                <h4 class="font-alt mt0">Stay Connect</h4>
                <a href="https://www.facebook.com/artlivemedia" class="text-muted mr15" data-toggle="tooltip" title="Facebook"><i class="ico-facebook2"></i></a>
                <a href="https://twitter.com/artlivemedia" class="text-muted mr15" data-toggle="tooltip" title="Twitter"><i class="ico-twitter2"></i></a>
                <a href="https://www.linkedin.com/company/artlivemedia" class="text-muted mr15" data-toggle="tooltip" title="LinkedIn"><i class="ico-linkedin2"></i></a>
<!--                <a href="javascript:void(0);" class="text-muted mr15" data-toggle="tooltip" title="Vimeo"><i class="ico-vimeo"></i></a>-->
<!--                <a href="javascript:void(0);" class="text-muted mr15" data-toggle="tooltip" title="Flickr"><i class="ico-flickr2"></i></a>-->
<!--                <a href="javascript:void(0);" class="text-muted mr15" data-toggle="tooltip" title="Google+"><i class="ico-google-plus2"></i></a>-->
<!--                <a href="javascript:void(0);" class="text-muted mr15" data-toggle="tooltip" title="Instagram"><i class="ico-instagram2"></i></a>-->
                */ ?>
            </div>
            <!--/ Address + Social -->

            <?php /*
            <!-- Newsletter -->
            <div class="col-md-4">
                <h4 class="font-alt mt0">Subscribe to beta?</h4>
                <form role="form" action="/site/subscribe_newsletter" method="post" name="frm_site_subscribe_newsletter" id="frm_site_subscribe_newsletter" class="">
                    <div class="form-group">
                        <p class="form-control-static">Want to subscribe to this closed beta?  Let us know here</p>
                    </div>
                    <div class="form-group message-container"></div><!-- will be use as done/fail message container -->
                    <div class="form-group">
                        <input type="email" class="form-control email" name="newsletter_email" id="newsletter_email" placeholder="Enter email">
                    </div>
                    <button type="submit" class="btn btn-primary btn-block btn-submit">Subscribe Now</button>
                </form>
            </div>
            <!--/ Newsletter -->
            */ ?>

        </div>
        <!--/ row -->
    </div>
    <!--/ container -->

    <!-- bottom footer -->
<div class="footer-bottom pt15 pb15 bgcolor-dark bgcolor-dark-darken10">
    <!-- container -->
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <!-- copyright -->
                <p class="nm text-muted">&copy; Copyright 2014 by <a href="//qpma.org.au" class="text-white">QPMA</a>. All Rights Reserved.</p>
                <!--/ copyright -->
            </div>
            <div class="col-sm-6 text-right">
<!--                <a href="javascript:void(0);" class="text-white">Privacy Policy</a>-->
<!--                <span class="ml5 mr5">&#8226;</span>-->
<!--                <a href="javascript:void(0);" class="text-white">Terms of Service</a>-->
            </div>
        </div>
    </div>
    <!--/ container -->
</div>
<!--/ bottom footer -->
</footer>
<!--/ END Template Footer -->

<?php $this->load->view('trackduck_tpl'); ?>

<!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
<!-- Library script : mandatory -->
<!--<script type="text/javascript" src="/assets/library/jquery/js/jquery.min.js"></script>-->
<!--<script type="text/javascript" src="/assets/library/jquery/js/jquery-migrate.min.js"></script>-->
<!--<script type="text/javascript" src="/assets/library/bootstrap/js/bootstrap.min.js"></script>-->
<script type="text/javascript" src="/assets/javascript/vendor.js"></script>
<script type="text/javascript" src="/assets/library/core/js/core.js"></script>
<script type="text/javascript" src="/assets/javascript/app.js"></script>
<!--/ Library script -->

<script>
    //var bsTooltip = $.fn.tooltip.noConflict();
    //$.fn.bootstrapTip = $.fn.tooltip.noConflict();
</script>

<!-- App and page level script -->
<!--<script type="text/javascript" src="/assets/plugins/sparkline/js/jquery.sparkline.min.js"></script><!-- will be use globaly as a summary on sidebar menu -->-->
<!--<script type="text/javascript" src="/assets/javascript/app.min.js"></script>-->

<script type="text/javascript" src="/assets/plugins/selectize/js/selectize.js"></script>

<script src="/assets/javascript/jstz-1.0.4.min.js"></script>

<script type="text/javascript" src="/assets/plugins/jqueryform/jquery.form.js"></script>

<script type="text/javascript" src="/assets/javascript/main.js"></script>
<script type="text/javascript" src="/assets/javascript/common.lib.js"></script>
<script type="text/javascript" src="/assets/javascript/common_validation.js"></script>

<script type="text/javascript" src="/assets/javascript/site.js"></script>
<script type="text/javascript" src="/assets/javascript/help_topics.js"></script>



<!--/ App and page level scrip -->
<!--/ END JAVASCRIPT SECTION -->
</body>
<!--/ END Body -->
</html>