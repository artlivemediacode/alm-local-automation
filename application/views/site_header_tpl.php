<!DOCTYPE html>
<html class="frontend">
<!-- START Head -->
<head>
    <!-- START META SECTION -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>QPMA</title>
    <meta property="og:title" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/assets/image/touch/apple-touch-icon-144x144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/assets/image/touch/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/assets/image/touch/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/assets/image/touch/apple-touch-icon-57x57-precomposed.png">
    <link rel="shortcut icon" href="/assets/image/favicon.ico">
    <!--/ END META SECTION -->

    <!-- START STYLESHEETS -->
    <!-- Plugins stylesheet : optional -->
    <link rel="stylesheet" href="/assets/plugins/selectize/css/selectize.min.css">

    <!--/ Plugins stylesheet -->

    <!-- Application stylesheet : mandatory -->
    <!--    <link rel="stylesheet" href="/assets/library/bootstrap/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="/assets/stylesheet/bootstrap.css">
    <link rel="stylesheet" href="/assets/stylesheet/layout.css">
    <link rel="stylesheet" href="/assets/stylesheet/uielement.min.css">
    <link rel="stylesheet" href="/assets/stylesheet/themes/layouts/fixed-header.css">
    <!--/ Application stylesheet -->
    <!-- END STYLESHEETS -->

    <link rel="stylesheet" href="/assets/stylesheet/custom.css">

    <!-- START JAVASCRIPT SECTION - Load only modernizr script here -->
    <script src="/assets/library/modernizr/js/modernizr.min.js"></script>
    <!--/ END JAVASCRIPT SECTION -->
</head>
<!--/ END Head -->

<!-- START Body -->
<body>
<?php //$this->load->view('g_analytics'); ?>
<!-- START Template Header -->
<header id="header" class="navbar ">
<div class="container">
<!-- START navbar header -->
<div class="navbar-header navbar-header-transparent">
    <!-- Brand -->
    <a class="navbar-brand mt5" href="javascript:void(0);">
<!--        <span class="functionmate-logo"></span>-->
        <img class="img" src="/assets/image/logo/full-logo.png" alt="" width="110" height="">
    </a>
    <!--/ Brand -->
</div>
<!--/ END navbar header -->

<!-- START Toolbar -->
<div class="navbar-toolbar clearfix">
    <!-- START Left nav -->
    <ul class="nav navbar-nav">
        <!-- Navbar collapse: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
        <li class="navbar-main navbar-toggle">
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#navbar-collapse">
<!--                <span class="meta">-->
<!--                    <span class="icon"><i class="ico-paragraph-justify3"></i></span>-->
<!--                </span>-->
            </a>
        </li>
        <!--/ Navbar collapse -->
    </ul>
    <!--/ END Left nav -->

    <!-- START navbar form -->
    <div class="navbar-form navbar-left dropdown" id="dropdown-form">
        <form action="" role="search">
            <div class="has-icon">
                <input type="text" class="form-control" placeholder="Search this site...">
                <i class="ico-search form-control-icon"></i>
            </div>
        </form>
    </div>
    <!-- START navbar form -->

    <?php /*
    <!-- START Right nav -->
    <ul class="nav navbar-nav navbar-right">
        <!-- Search form toggler -->
        <li>
            <a href="javascript:void(0);" data-toggle="dropdown" data-target="#dropdown-form">
                                <span class="meta">
                                    <span class="icon"><i class="ico-search"></i></span>
                                </span>
            </a>
        </li>
        <!--/ Search form toggler -->
    </ul>
    <!--/ END Right nav -->
    */ ?>
    <!-- START nav collapse -->
    <div class="collapse navbar-collapse navbar-collapse-alt" id="navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
            <li class="">
                <?php if($this->common->isValidSession()): ?>
                    <a href="/dashboard" class="<?php echo ($this->router->class == 'dashboard')?'active':''; ?>">
                            <span class="meta">
                                <span class="text">DASHBOARD</span>
                            </span>
                    </a>
                <?php endif; ?>
            </li>

            <li class="">
                <a href="/help" class="<?php echo ($this->router->class == 'help')?'active':''; ?>">
                    <span class="meta">
                        <span class="text">HELP</span>
                    </span>
                </a>
            </li>
        </ul>
    </div>
    <!--/ END nav collapse -->
</div>
<!--/ END Toolbar -->
</div>
</header>
<!--/ END Template Header -->