<VirtualHost 127.0.0.1:80>
    ServerAdmin webmaster@dummy-host2.example.com
    DocumentRoot "<?php echo $doc_root; ?>"
    ServerName "<?php echo $localhost; ?>"
    #ServerAlias "<?php echo $localhost_alias; ?>"
    <Directory "<?php echo $doc_root; ?>">
        AuthType None
        Options All
        AllowOverride All
        Require all granted
        Satisfy Any
    </Directory>
    ErrorLog "logs/<?php echo $localhost; ?>-error.log"
    CustomLog "logs/<?php echo $localhost; ?>-access.log" common
</VirtualHost>