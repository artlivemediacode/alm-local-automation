$(document).ready(function() {


});

/////////////FORM VALIDATIONS///////////////

/////////////PRE HOOKS///////////////
function AMpre_category_init_cat_update(thisEle)
{
    var cat_type = thisEle.data('type');

    /*if(thisEle.hasClass('active')) {
        return false;
    }*/

    var options = {
        cat_type:cat_type
    };

    //thisEle.addClass('active');
    return options;
}

function AMpre_category_delete_cat(thisEle)
{
    var cat_id = thisEle.data('id');
    var cat_type = thisEle.data('type');

    if(thisEle.hasClass('active')) {
        return false;
    }
    if(thisEle.hasClass('validated')) {

    } else {
        //alert("wegeg")
        var return_var = false;

        var confirm_data = {};
        confirm_data['c_msg'] = "Are you sure want to delete?";
        confirm_data['title'] = 'Confirm Delete';
        confirm_data['validation_obj'] = thisEle;
        confirm_data['v_obj_trigger'] = 'click';
        confirm_data['remove_anim'] = true;

        bsConfirmPopup(confirm_data);
        return false;
    }

    if(return_var) {
        thisEle.removeClass('validated');
    }

    var options = {
        cat_id:cat_id,
        cat_type:cat_type
    };

    thisEle.addClass('active');
    return options;
}

/*function AMpre_category_delete_tag(thisEle)
{
    var tag_id = thisEle.data('id');
    var tag_type = thisEle.data('type');

    if(thisEle.hasClass('active')) {
        return false;
    }
    if(thisEle.hasClass('validated')) {

    } else {
        //alert("wegeg")
        var return_var = false;

        var confirm_data = {};
        confirm_data['c_msg'] = "Are you sure want to delete?";
        confirm_data['title'] = 'Confirm Delete';
        confirm_data['validation_obj'] = thisEle;
        confirm_data['v_obj_trigger'] = 'click';
        confirm_data['remove_anim'] = true;

        bsConfirmPopup(confirm_data);
        return false;
    }

    if(return_var) {
        thisEle.removeClass('validated');
    }

    var options = {
        tag_id:tag_id,
        tag_type:tag_type
    };

    thisEle.addClass('active');
    return options;
}*/

//////////CALLBACKS/////////////////
function AMcallBack_category_init_cat_update(data)
{

    if(data.DATA.success) {
        $('.cat_create_n_update').remove();

        var $modal = $(data.HTML).filter('.modal');
        $('body').append($modal);

        $modal = $('.cat_create_n_update');

        //return false;
        //Bind Callback functions with model events
        $modal.on('show.bs.modal', function() {
            //console.log('Modal will be shown');
        }).on('shown.bs.modal', function() {
            //console.log('Modal was shown');
            //init_body_shape_popup();
        }).on('hide.bs.modal', function() {
            //console.log('Modal will be hidden');
        }).on('hidden.bs.modal', function() {
            //console.log('Modal is hidden');
            $('.cat_create_n_update').remove();
        });

        $modal.modal({
            backdrop: true,
            keyboard: true
        });

        //close_pop_loading();
        $modal.modal('show');
    } else {

    }
}

function AMcallBack_category_cat_create_n_update(data)
{
    $('#frm_cat_create_n_update').children().find('.btn-submit').html('Create');
    if(data.DATA.is_update) {
        $('#frm_cat_create_n_update').children().find('.btn-submit').html('Update');
    }

    $('#frm_cat_create_n_update').removeData('disabled');

    $('#frm_cat_create_n_update .form-group').removeClass('has-error').removeClass('has-success');

    if(data.DATA.success) {
        var succes_alert = '<div class="alert alert-success ma5">Successfully created.</div>';
        if(data.DATA.is_update) {
            succes_alert = '<div class="alert alert-success ma5">Successfully updated.</div>';
        } else {
            //
            var html = '<li class="list-group-item" data-id="'+data.DATA.cat_vals.cat_id+'">';
            html += '<span class="">'+data.DATA.cat_vals.cat_name+'</span>';
            html += '<a href="/help/delete_cat" class="text-danger pull-right ajaxme" data-id="'+data.DATA.cat_vals.cat_id+'"><i class="icon ico-remove3"></i></a>';
            html += '</li>';
            $('.cats_list_row ul').prepend(html);
            //

            $('#frm_cat_create_n_update input[type="text"]').val('');
            //$('#frm_cat_create_n_update textarea').val('');
        }
        $('#frm_cat_create_n_update .frm_alert_hld').html(succes_alert).removeClass('hide');

        //$('form#menu_items_cat_filter').submit();
    } else {
        theme_form_err_msg(data.DATA.error_msg, '#frm_cat_create_n_update');
    }
}

function AMcallBack_category_delete_cat(data)
{
    if(data.DATA.success) {
        //$('form#menu_items_cat_filter').submit();

        //alert(data.DATA.cat_id)
        //alert($('.menu_cats_list_row ul li[data-id="'+data.DATA.cat_id+'"]').length)
        $('.cats_list_row ul li[data-id="'+data.DATA.cat_id+'"]').remove();

    } else {
        theme_form_err_msg(data.DATA.error_msg, '#frm_m_item_cat_create_n_update');
    }
}
