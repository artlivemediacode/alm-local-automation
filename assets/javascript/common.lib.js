var pageHash = 1;
var AMG = {
    fb_scope:'publish_actions,user_friends,friends_photos,friends_relationships,email,read_stream,' +
        'publish_stream,user_birthday,user_location,user_work_history,user_hometown,' +
        'user_photos,offline_access,user_videos',
    //static_url:'https://staticsoh.s3-ap-southeast-2.amazonaws.com',
    static_url:'',
    AJAXOBJ:{}
};
//var my_id = $('footer#footer').data('my_id');
//var stripe_pub = $('footer').data('stripe_pub');
var stripe_pub = $('#stripe_pub').val();

if(typeof Stripe !== 'undefined' && stripe_pub !== '') {
    Stripe.setPublishableKey(stripe_pub);
}

$(document).ready(function(){
    //alert("wegweg")
    /*$(window).bind('hashchange', function (e) {
        pageIndex = e.getState("pageIndex") || '/user';
        if(pageHash)
        	initURL(pageIndex);
        pageHash = 1;
    });*/
	
    $('body').delegate('.searchElements','keyup',function(e) {
		switch (e.which)
		{
			case 40:
			case 38:
			case 13:
				return true;
		}
		//alert("wef");
		//var url = $(this).attr('url');
        var url = $(this).data('url');
		if(url === '' || typeof(url) === 'undefined' || $(this).hasClass('direct'))
            return true;
        if($(this).hasClass('action')) {
            return true;
        }
		
		if(isDefined(AMG.AJAXOBJ.search)) {//aborting prerunning search requests
			AMG.AJAXOBJ.search.abort();
        }
		
        var func = genPreFunction(url);
		
		if(isDefined($(this).attr('switch'))) {
			func = func+'_'+$(this).attr('switch');
		}

		e.preventDefault();
		var preSubmitHook ="AMpre"+func;
		var val = {};
		if(typeof window[preSubmitHook] === 'function') {
			
			//invoke the preSubmit function;
			val = window[preSubmitHook]($(this));
			if(val === false) {
                return false;
            }
		} else {
            val = {str:$(this).val()};
        }
		almAjax({
				URL:url,
				POST : val,
				AJAXOBJ:'search'
		});
		return false;
	});
    

	$('body').delegate('a.ajaxme,button.ajaxme','click',function(e) {
        var url = $(this).attr('href');
		if(url === '#') {
			return true;
		}
        if(isDefined($(this).attr('link'))) {
            url = $(this).attr('link');
        }
        if(isDefined($(this).attr('data-link'))) {
            url = $(this).attr('data-link');
        }

        if(!isDefined(url)) {
            return true;
        }
		var history = $(this).attr('history');
		var slider = $(this).attr('slider');
		if(url == '' || typeof(url) == 'undefined' || $(this).hasClass('direct')) {
            return true;
        }

        if($(this).hasClass('action')) {
            return true;
        }

        e.preventDefault();
        var func = genPreFunction(url);
        if(isDefined($(this).attr('switch'))) {
			func = func+'_'+$(this).attr('switch');
		}

		var preSubmitHook = "AMpre"+func;
		var val = {};
		if(typeof window[preSubmitHook] === 'function') {
			//invoke the preSubmit function;
			val = window[preSubmitHook]($(this));
			if(val === false) {
                return false;
            }
		}
		
		if((typeof(history) === 'undefined' || typeof(slider) !== 'undefined') && history !== 0) {
			//$.bbq.pushState({ pageIndex: url}); //making browser history
			pageHash = 0;
		}
		almAjax({
				URL:url,
				POST : val
		});
		return false;
	});

	//$('body').undelegate('form.ajaxme','submit').delegate('form.ajaxme','submit',function(e) {
    $('body').undelegate('form','submit').delegate('form','submit',function(e) {
        if($(this).hasClass('norm')) {
            return true;
        }

        if($(this).attr('disabled') === 'disabled') {
            return false;
        }

		//alert("wef");
		e.preventDefault();
		
		//var myForm =  $(this).parents('form:first');
		//var myURL = $(this).attr('action');
		var myFormName = $(this).attr('name');

		// call the pre submit hook
		var preSubmitHook = myFormName + "_validation";
		if(typeof window[preSubmitHook] === 'function') {
			//invoke the preSubmit function;
			var val = window[preSubmitHook]($(this));
			if(val === false) {
                return false;
            }
		}
		
		var options = {
				type:'POST',
				dataType:'json',
				data:{"HTTP_X_REQUESTED_WITH":"xmlhttprequest"},
				//iframeSrc:window.location.href,
				success:function(data) {
					almAjaxCallBack(data);
				}	
		};
	
		//almAjax(options);

		/*if( $(this).children('input[name=SHOWLOADING]').val() == '1') AMG.LOADING++;
		if(AMG.LOADING>0) 
		{
			$('.footerNav span').addClass('active');
		}*/

		$(this).ajaxSubmit(options);
		
		return false;

	});

	//window.onerror = stopError;
});

function genPreFunction(url)
{
	var str = url;
	//var get = '';
	
	if(strpos(url, '?', 0)) {
		str = url.substr(0, strpos(url, '?', 0));
	}
	
	//alert(get);
	str = str.split('/');

	var func = '';
	for(var i = 0;i < str.length;i++) {
		if(str[i] !== '') {
			func = func + '_' + str[i];
		}
	}
	return func;
}

function almAjax (options) {
	
	var URL = isDefined(options.URL) ? options.URL : '/site/dummy';
	this.GET = isDefined(options.GET) ? options.GET : {};
	this.POST = isDefined(options.POST) ? options.POST : {};
	this.POST.LOCK = isDefined(options.LOCK) ? options.LOCK : '0';
	this.POST.SHOWLOADING = isDefined(options.SHOWLOADING) ? options.SHOWLOADING : 1;
    //this.POST['CB_SWITCH'] = isDefined(options['CB_SWITCH']) ? '_'+options['CB_SWITCH'] : '';
	
	var ajaxObj = isDefined(options.AJAXOBJ) ? options.AJAXOBJ : 'SITE';

	if(this.POST.SHOWLOADING === 1) {
        AMG.LOADING++;
    }
	//if(AMG.LOADING > 0) {
		//$('.footerNav span').addClass('active');
	//}

	var urlGET = '';
	var count = 0;
	for(var key in this.GET) {
		if(count !== 0) {
            urlGET += '&';
        }
		urlGET += key + '=' + this.GET[key];
		count++;
	}

	var urlString = URL;
	if(count > 0) {
		urlString += '?' + urlGET;
	}

	if(!isDefined(AMG.AJAXOBJ)) {
		AMG.AJAXOBJ = {};
	}

	AMG.AJAXOBJ[ajaxObj] = $.post(urlString,this.POST,function(data){
		almAjaxCallBack(data);
	}).fail(function(xhr, textStatus, errorThrown) {
        var data = {};
        data['DATA'] = {success:false};
        data['error_msg'] = {global:xhr.responseText};
        almAjaxCallBack(data);
    });

	return AMG.AJAXOBJ[ajaxObj];
}

function almAjaxCallBack(data) {
	var rData;
	if(typeof data.MESSAGE === 'undefined') {
        try {
            rData = $.parseJSON(data);
        } catch(err) {
            return false;
        }
    } else {
        rData = data;
    }
    //console.dir(rData);

    if(typeof rData.CB === 'undefined') {
        return false;
    }
	//var rData = data;
	var cbFuncName = 'AMcallBack_' + rData.CB;
	if(rData.SHOWLOADING === 1)  {
		AMG.LOADING--;
		//if(AMG.LOADING==0)  $('.footerNav span').removeClass('active');
	}

    //alert(cbFuncName);

	//if(rData.MESSAGE != '')
		//AMshowAJAXMessage(rData.MESSAGE,rData.RESULT);

	if(typeof window[cbFuncName] !== 'function') {
		alert('Invalid ajax return type ' + cbFuncName);
		return;
	}

    if(typeof rData.DATA.requires_login !== 'undefined' && rData.DATA.requires_login) {
        bootbox.dialog({
            message: "Please login again to continue.",
            title: "Session expired.",
            closeButton: false,
            buttons: {
                main: {
                    label: "OK",
                    className: "btn-primary",
                    callback: function() {
                        window.location = '/site';
                    }
                }
            }
        });
    }

	if(typeof rData.DATA.no_perm !== 'undefined' && rData.DATA.no_perm) {
		bootbox.dialog({
			message: "Sorry you don't have the permission.",
			title: "Access Denied.",
			closeButton: false,
			buttons: {
				main: {
					label: "OK",
					className: "btn-primary",
					callback: function() {
						window.location = '/site';
					}
				}
			}
		});
	}

    //invoke the callback function;
	window[cbFuncName](rData);
}
