function validateEmail(email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if( !emailReg.test( email ) ) {
        return false;
    } else {
        return true;
    }
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
function isDate(val) {
    var d = new Date(val);
    return !isNaN(d.valueOf());
}