$( document ).ready(function() {
    $('#feed_url').val(document.URL);

    $('#contact-wrapper').stickyfloat({ duration: 400, cssTransition: true, offsetY:150}); //stickyfloat contact form

    $("#contact-btn").on('click',function() { //smoothly slide open/close form
        var floatbox = $("#floating-contact-wrap");

        if (floatbox.hasClass('visible')){

            floatbox.animate({
                                right:"-320px"
                            },600,function() {
                                // Animation complete.
                                //$("#contact-wrapper").css('width','44px');
                                //$("#contact-btn").animate({'margin-left':'-70px'},500)
                                $(this).removeClass('visible');
                            });

            //$('#anonymous').prop('checked', false);
            //$('#get_in_touch').prop('checked', false);
            //$('#feedback').val('');
            //$('#result').html('');

        }else{
            //$("#contact-wrapper").css('width','365px');
            //$("#contact-btn").css('margin','80px 0px 0px -50px');
            floatbox.animate({"right":"0px"}, "10",
                            function(){
                                //$(this)
                            }).addClass('visible');
        }


    });

    $("#frm_feedback #anonymous").on('change',function() {
        $('#frm_feedback .get_in_touch_hld').removeClass('hide');
        if($(this).is(':checked') == true ){
            $('#frm_feedback .get_in_touch_hld').addClass('hide');
        }

    });
});

/////////////FORM VALIDATIONS///////////////
function frm_feedback_validation(thisForm)
{
    var return_var = true;

    if(typeof thisForm.data('disabled') !== 'undefined') {
        return_var = false;
    }

    var feedback_ele = thisForm.children().find('.feedback');

    var feedback = feedback_ele.val();

    var error_list = [];
    var frm_alert_hld = thisForm.children().find('.frm_alert_hld');
    frm_alert_hld.html('');

    if(feedback === '') {
        error_list.push('Please enter feedback.');
        add_frm_element_err(feedback_ele);
        return_var = false;
    } else {
        remove_frm_element_err(feedback_ele);
    }

    $.each(error_list,function(i,v){
        frm_alert_hld.append('<div class="alert alert-danger">'+v+'</div>');
    });

    frm_alert_hld.removeClass('hide');
    if(return_var) {
        frm_alert_hld.addClass('hide');
        thisForm.data('disabled',true);
        thisForm.children('.btn-submit').html('Please wait..');
    }

    return return_var;
}

//////////CALLBACKS/////////////////
function AMcallBack_feedback_get_feedback(data)
{
    $('#frm_feedback .btn-submit').html('Submit');
    $('#frm_feedback').removeData('disabled');

    if(data.DATA.success) {
        //$('#result').html(data.DATA.msg).addClass('text-success');
        var succes_alert = "<div class='alert alert-success'>"+data.DATA.msg+"</div>";
        $('#frm_feedback .frm_alert_hld').html(succes_alert).removeClass('hide');

        $('#frm_feedback textarea').val('');

    } else {
        var info_data = {};
        info_data['c_msg'] = 'Unknown error occured. Please try again later.';
        info_data['title'] = 'Feedback Error';
        bsInfoPopup(info_data);
    }
}
