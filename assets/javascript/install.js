$(function() {
    //return false;
    init_application();
});

function init_application()
{
    $('body').on('keydown','.project_name',function(e) {

        /*var regex = new RegExp("^[a-zA-Z0-9]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }*/

        var key = e.charCode || e.keyCode || 0;
        // allow backspace, tab, delete, arrows, letters, numbers and keypad numbers ONLY
        return (
        key == 8 ||
        key == 9 ||
        key == 46 ||
        (key >= 37 && key <= 40) ||
        (key >= 48 && key <= 57) ||
        (key >= 65 && key <= 90) ||
        (key >= 96 && key <= 105));

        e.preventDefault();
        return false;
    });

    $('body').on('keyup','.project_name',function() {
        var project_name = $(this).val();
        if($.trim(project_name) == '') {
            $(this).closest('form').children().find('.localhost').val('');
            $(this).closest('form').children().find('.proj_name_clone').val('');

        } else {
            $(this).closest('form').children().find('.localhost').val('loc.'+project_name+'.com');
            $(this).closest('form').children().find('.proj_name_clone').html('/'+project_name);
        }
        //return false;
    });

    /*$('body').on('change','.hosting',function() {
        //alert($(this).val());
        user_dir_check();
        //return false;
    });*/

    $('body').on('change','.is_wpengine',function() {
        //alert($(this).val());
        user_dir_check();


        $('.email_hld').addClass('hide');
        if($(this).is(':checked')) {
            $('.email_hld').removeClass('hide');
        } else {
        }
        //return false;
    });

    $('body').on('change','.is_git',function() {
        //alert($(this).val());
        user_dir_check();
        //return false;
    });

    $('body').on('change','.build',function() {
        //alert($(this).val());
        //$('.hosting_hld').removeClass('hide');
        $('.wpengine_hld').removeClass('hide');
        if($(this).val() == 'custom') {
            //$('.hosting').prop("checked", false);
            //$('.hosting_hld').addClass('hide');
            $('.is_wpengine').prop("checked", false);
            $('.wpengine_hld').addClass('hide');
        } else {
        }
        user_dir_check();
        //return false;
    });

    /*$('body').on('change','.change_vhosts',function() {
        var input = $(this)[0];
        var fReader = new FileReader();
        fReader.readAsDataURL(input.files[0]);
        fReader.onloadend = function(event){
            console.dir(event);
        }

        console.dir(input);

        var vhosts = $(this).val();
        if($(this).val() == '') {
        } else {
            $('.vhosts').val(vhosts);
        }
        //return false;
    });*/


    $('body').on('keyup','#frm_delete_project .project_name',function() {
        var project_name = $(this).val();
        if($.trim(project_name) == '') {
            $('#frm_delete_project .database').val('');
            return;
        }
        project_name = 'wordpress_'+project_name;
        $('#frm_delete_project .database').val(project_name);
    });
}

function user_dir_check() {
    $('.user_dir_hld').addClass('hide');
    if($('.is_git:checked').val() == 'yes' && $('.is_wpengine:checked').val() == 'yes') {
        $('.user_dir_hld').removeClass('hide');
    } else {
    }
}

/*function email_check() {
    $('.email_hld').addClass('hide');
    if($('.is_wpengine:checked').val() == 'yes') {
        $('.email_hld').removeClass('hide');
    } else {
    }
}*/
/////////////PRE HOOKS///////////////
function AMpre_install_change_app_status(thisEle)
{
    /*if(typeof thisEle.data('disabled') !== 'undefined') {
     return false;
     }*/
    if(thisEle.hasClass('active')) {
        return false;
    }

    var return_var = true;
    thisEle.closest('.btn-group').removeClass('open');
    var app_status_disp = thisEle.html();

    if(thisEle.hasClass('validated')) {

    } else {
        var return_var = false;

        var confirm_data = {};
        confirm_data['c_msg'] = "Are you sure want to change application status to <strong>"+app_status_disp+"</strong>?";
        confirm_data['title'] = 'Change Application Status';
        confirm_data['validation_obj'] = thisEle;
        confirm_data['v_obj_trigger'] = 'click';
        confirm_data['remove_anim'] = true;

        bsConfirmPopup(confirm_data);
        return false;
    }

    if(return_var) {
        thisEle.removeClass('validated');
    }

    var app_status = thisEle.data('id');
    var app_id = thisEle.closest('.btn-group').data('id');

    var options = {
        app_id:app_id,
        app_status:app_status
    };

    showPopLoading();

    $('#app_status').attr('disabled', 'disabled');
    //return false;
    //thisEle.html('Please wait');
    //thisEle.data('disabled', true);
    return options;
}

/////////////FORM VALIDATIONS///////////////
function frm_install_validation(thisForm)
{
    var return_var = true;

    if(typeof thisForm.data('disabled') !== 'undefined') {
        return_var = false;
    }

    var project_name_ele = thisForm.children().find('.project_name');
    var project_path_ele = thisForm.children().find('.project_path');

    var localhost_ele = thisForm.children().find('.localhost');
    var build_ele = thisForm.children().find('.build');
    //var hosting_ele = thisForm.children().find('.hosting');
    var is_wpengine_ele = thisForm.children().find('.is_wpengine');
    var is_git_ele = thisForm.children().find('.is_git');
    var user_directory_ele = thisForm.children().find('.user_directory');

    var project_name = project_name_ele.val();
    var project_path = project_path_ele.val();

    var localhost = localhost_ele.val();
    var build = build_ele.val();
    //var hosting = hosting_ele.val();
    var is_wpengine = is_wpengine_ele.val();
    var is_git = is_git_ele.val();
    var user_directory = user_directory_ele.val();

    var error_list = [];
    var frm_alert_hld = thisForm.find('.frm_alert_hld');
    frm_alert_hld.html('');

    if(project_name == '' || project_name == null) {
        error_list.push('Please enter your project name.');
        add_frm_element_err(project_name_ele);
        return_var = false;
    } else {
        remove_frm_element_err(project_name_ele);
    }

    if(project_path == '' || project_path == null) {
        error_list.push('Please enter your project path.');
        add_frm_element_err(project_path_ele);
        return_var = false;
    } else {
        remove_frm_element_err(project_path_ele);
    }

    if(localhost == '' || localhost == null) {
        error_list.push('Please enter your localhost url.');
        add_frm_element_err(localhost_ele);
        return_var = false;
    } else {
        remove_frm_element_err(localhost_ele);
    }

    if(thisForm.children().find('.build:checked').length == 0) {
        error_list.push('Please choose your project build.');
        add_frm_element_err(build_ele);
        return_var = false;
    } else {
        remove_frm_element_err(build_ele);
    }

    /*if(thisForm.children().find('.hosting:checked').length == 0) {
        error_list.push('Please choose your project hosting.');
        add_frm_element_err(hosting_ele);
        return_var = false;
    } else {
        remove_frm_element_err(hosting_ele);
    }*/

    if(thisForm.children().find('.is_git:checked').length == 0) {
        error_list.push('Please choose if you want git setup.');
        add_frm_element_err(is_git_ele);
        return_var = false;
    } else {
        remove_frm_element_err(is_git_ele);
    }

    //if($('.hosting:checked').val() == 'wpengine' && $('.is_git:checked').val() == 'yes') {
    if($('.is_wpengine:checked').val() == 'yes') {
        $('.user_dir_hld').removeClass('hide');
        //console.dir(team_mems)
        if(user_directory == '' || user_directory == null) {
            error_list.push('Please select your user directory.');
            add_frm_element_err(user_directory_ele);
            return_var = false;
        } else {
            remove_frm_element_err(user_directory_ele);
        }
    }

    //return_var = false;
    $.each(error_list,function(i,v){
        frm_alert_hld.append('<div class="alert alert-danger">'+v+'</div>');
    });

    //alert("eg")

    frm_alert_hld.removeClass('hide');
    if(return_var) {
        frm_alert_hld.addClass('hide');
        //thisForm.data('disabled',true);
        thisForm.children().find('.btn-submit').html('Please wait..');
    }

    return return_var;
}

//////////CALLBACKS/////////////////
function AMcallBack_install_run(data)
{
    /*$('#frm_create_update_app').children().find('.btn-submit').html('Create');
    if(data.DATA.is_update) {
        $('#frm_create_update_app').children().find('.btn-submit').html('Update');
    }*/
    $('#frm_install').children().find('.btn-submit').html('RUN!');

    $('#frm_install').removeData('disabled');

    $('#frm_install .form-group').removeClass('has-error').removeClass('has-success');

    if(data.DATA.success) {
        var succes_alert = '<div class="alert alert-success ma5">Successfully created. Restart Apache <strong>'+data.DATA.bitnami_mngr_path+'</strong> and open <a href="http://'+data.DATA.localhost+'" target="_blank">'+data.DATA.localhost+'</a></div>';
        $('#frm_install .frm_alert_hld').html(succes_alert).removeClass('hide');

        //window.open(url, '_blank');
    } else {
        theme_form_err_msg(data.DATA.error_msg, '#frm_install');
        $('#frm_install').removeData('disabled');
    }

    var container = $('html,body'),
        scrollTo = $('#frm_install .frm_alert_hld');
    container.animate({
        scrollTop: 0
    });
}
