$(document).ready(function() {
    //var bsTooltip = $.fn.tooltip.noConflict();

    //$('[data-toggle="tooltip"]').tooltip({'placement': 'bottom'});
    //$("body").tooltip({ selector: '[data-toggle="tooltip"]' });
    //alert(typeof window['bootstrapTip'])
    //if(typeof window['bootstrapTip'] === 'function') {
    if($.isFunction($.fn.bootstrapTip)) {
        $('[data-toggle=tooltip]').bootstrapTip();
    }
    //alert($('[rel=tooltip]').length)

    /*$(".dt-picker").datepicker({
        dateFormat: 'd MM yy',
    });*/
    if($(".selectize").length) {
        $(".selectize").selectize({
            onChange: function (e) {
                //var data = this.options[e];
            }
        });
    }

    $('body').on('change keyup','.auto_submit',function(e){
        if(!isDefined($(this).closest('form'))) {
            return true;
        }
        //console.dir(e)
        if(e.type == 'keyup') {

        }

        var form = $(this).closest('form');
        form.children('.pageid').val(1);
        form.submit();
    });

    $('.navbar-form .ico-search').on('click',function(){
        $(this).closest('form').submit();
    });

    $('table .sort_col').on('click',function(){
        if($(this).hasClass('active')){
            return false;
        }
        var o_by = $(this).data('o_by');
        var o_dir = (typeof $(this).data('o_dir') == 'undefined')?'ASC':'DESC';
        var new_o_dir = ($(this).data('o_dir') == 'ASC')?'DESC':'ASC';
        $(this).data('o_dir', new_o_dir);
        var table = $(this).closest('table');
        var form_id = $(this).closest('table').data('form_id');
        var form = $('#'+form_id);

        form.children('.order_by').val(o_by);
        form.children('.order_dir').val(new_o_dir);

        table.children().find('.sort_col').removeClass('headerSortDown').removeClass('headerSortUp');

        if(new_o_dir == 'ASC') {
            $(this).addClass('headerSortDown');
        } else {
            $(this).addClass('headerSortUp');
        }

        $(this).closest('.panel').children('.indicator').addClass('show');
        //table.children().find('.sort_col').addClass('active');
        form.submit();
    });
});

function theme_form_err_msg(error_msg, form, filt_data)
{
    if(typeof error_msg !== 'undefined') {
        var error_list = '';
        $.each(error_msg,function(i,v){
            if($.trim(v) === '') {
                return;
            }

            var msg_before = (typeof filt_data !== 'undefined' && typeof filt_data.msg_before !== 'undefined')?filt_data.msg_before:'<div class="alert alert-danger ma5">';
            var msg_after = (typeof filt_data !== 'undefined' && typeof filt_data.msg_after !== 'undefined')?filt_data.msg_after:'</div>';

            error_list += msg_before+$.trim(v)+msg_after;

            if($(form+' .'+i+'').length)
                $(form+' .'+i+'').closest('.form-group').addClass('has-error').removeClass('has-success');
            if($(form+' #'+i+'').length)
                $(form+' #'+i+'').closest('.form-group').addClass('has-error').removeClass('has-success');
        });

        $(form+' .frm_alert_hld').html(error_list).removeClass('hide');
    }
}

function add_frm_element_err(ele)
{
    ele.closest('.form-group').addClass('has-error').removeClass('has-success');
}

function remove_frm_element_err(ele)
{
    ele.closest('.form-group').addClass('has-success').removeClass('has-error');
}

function showPopLoading() {
    $('.page_load_indicator').remove();

    $('body').append('<div class="indicator show page_load_indicator"><span class="spinner spinner3" style=""></span></div>');
    var sh = parseInt($(document).scrollTop()) + 200;
    $('.page_load_indicator').children('.spinner').stop().animate({top:sh+'px'});

    $(window).on('scroll', function(){
       if($('.page_load_indicator').length) {
           var sh = parseInt($(document).scrollTop());
           $('.page_load_indicator').children('.spinner').stop().animate({top:sh+'px'});
       }
    });
    return;

    $('body').append('<div class="modal pop_loading"><img src="/assets/img/ajax-loader.gif"></div>');
    var modal = $('.modal.pop_loading');

    modal.on('show', function() {
        //console.log('Modal will be shown');
    }).on('shown', function() {
            //console.log('Modal was shown');
            //init_body_shape_popup();

        }).on('hide', function() {
            //console.log('Modal will be hidden');
        }).on('hidden', function() {
            //console.log('Modal is hidden');
            $('.modal.pop_loading').remove();
        }).show({
            backdrop: true,
            keyboard: true
        });

    //$('.modal.pop_loading').modal('show');
    modal.modal('show');

    //modal.modal('lock');
}

function closePopLoading() {
    $('.page_load_indicator').remove();
    return;

    var modal = $('.modal.pop_loading');
    modal.modal('unlock');
    modal.modal('hide');
}

function preload(sources, callback) {
    if(sources.length) {
        var preloaderDiv = $('<div style="display: none;"></div>').prependTo(document.body);

        $.each(sources, function(i,source) {
            $("<img/>").attr("src", source).appendTo(preloaderDiv);

            if(i === (sources.length-1)) {
                $(preloaderDiv).imagesLoaded(function() {
                    $(this).remove();
                    if(callback) {
                        callback();
                    }
                });
            }
        });
    } else {
        if(callback) {
            callback();
        }
    }

    /*for(var i = 0; i<arguments.length; i++)
     {
     $("<img>").attr("src", arguments[i]);
     }*/
}

function bsConfirmPopup(data)
{
    //console.dir(data);
    //return;

    var thisForm = (typeof data.thisForm === 'undefined')?'':data.thisForm;
    var c_msg = (typeof data.c_msg === 'undefined')?'Are you sure?':data.c_msg;
    var title = (typeof data.title === 'undefined')?'Confirm':data.title;
    var validation_obj = (typeof data.validation_obj === 'undefined')?'':data.validation_obj;
    var v_obj_trigger = (typeof data.v_obj_trigger === 'undefined')?'':data.v_obj_trigger;
    var cb = (typeof data.cb === 'undefined')?'':data.cb;

    var box = bootbox.dialog({
        message: c_msg,
        title: title,
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default",
                callback: function () {
                    // callback
                    //this.close();
                    box.modal('hide');

                    if (thisForm !== '') {
                        thisForm.removeClass('validated').removeClass('validated_twice');
                    }

                    if (validation_obj !== '') {
                        validation_obj.removeClass('validated');
                    }
                }
            },
            success: {
                label: "Yes",
                className: "btn-primary",
                callback: function () {
                    //this.close();
                    box.modal('hide');

                    if (validation_obj !== '') {
                        validation_obj.addClass('validated');
                    }

                    if (thisForm !== '') {
                        thisForm.submit();
                    }

                    if (cb !== '' && typeof window[cb] === 'function') {
                        //invoke the callback function;
                        var cb_param = (typeof data.cb_param === 'undefined')?'':data.cb_param;
                        if(cb_param === '') {
                            window[cb]();
                        } else {
                            window[cb](cb_param);
                        }
                        return;
                    }

                    if (validation_obj !== '' && v_obj_trigger !== '') {
                        validation_obj.trigger(v_obj_trigger);
                    }
                }
            }
        }
    });

    /*var dialog = new BootstrapDialog({
        title	:	title,
        message	:	c_msg,
        onshow: function(dialog) {
            //console.dir(dialog);
            //var dialog_dom = dialog.getDialog();
            var modal = dialog.$modal;
            modal.addClass('dataConfirmModal confirm');

            //Disables animation
            if(typeof data.remove_anim !== 'undefined') {
                modal.removeClass('fade');
            }

            modal.removeClass('hide');
        },
        buttons	:	[{
            label	:	'Cancel',
            action	:	function(dialog){
                dialog.close();

                if (thisForm !== '') {
                    thisForm.removeClass('validated').removeClass('validated_twice');
                }

                if (validation_obj !== '') {
                    validation_obj.removeClass('validated');
                }
            }
        }, {
            label	:	'Yes',
            cssClass:	'btn-primary',
            action	:	function(dialog){
                dialog.close();

                if (validation_obj !== '') {
                    validation_obj.addClass('validated');
                }

                if (thisForm !== '') {
                    thisForm.submit();
                }

                if (cb !== '' && typeof window[cb] === 'function') {
                    //invoke the callback function;
                    var cb_param = (typeof data.cb_param === 'undefined')?'':data.cb_param;
                    if(cb_param === '') {
                        window[cb]();
                    }
                    else {
                        window[cb](cb_param);
                    }

                    return;
                }

                if (validation_obj !== '' && v_obj_trigger !== '') {
                    validation_obj.trigger(v_obj_trigger);
                }
            }
        }]
    });
    dialog.open();*/

    /*dialog.setReady(function(dialog){
        // Disable the button before the dialog is popped up
        //dialog.getButton('btn-close').attr('disabled', 'disabled');

        var dialog_dom = dialog.getDialog();
        dialog_dom.addClass('dataConfirmModal confirm');

        //Disables animation
        if(typeof data.remove_anim!='undefined')
            dialog_dom.removeClass('fade');

        dialog_dom.removeClass('hide');
    });*/

}

function bsInfoPopup(data)
{
    //console.dir(data);
    //return;

    //var thisForm = (typeof data.thisForm==='undefined')?'':data.thisForm;
    var c_msg = (typeof data.c_msg === 'undefined')?'Are you sure?':data.c_msg;
    var title = (typeof data.title === 'undefined')?'Confirm':data.title;
    //var validation_obj = (typeof data.validation_obj==='undefined')?'':data.validation_obj;
    //var v_obj_trigger = (typeof data.v_obj_trigger==='undefined')?'':data.v_obj_trigger;
    //var cb = (typeof data.cb==='undefined')?'':data.cb;

    bootbox.dialog({
        message: c_msg,
        title: title,
        buttons: {
            ok: {
                label: "Ok",
                className: "btn-default",
                callback: function () {
                    // callback
                    this.close();
                }
            }
        }
    }).on('shown.bs.modal', function(){
        var modal = this; //dialog.$modal;
        modal.addClass('dataConfirmModal confirm');

        //Disables animation
        if(typeof data.remove_anim !== 'undefined') {
            modal.removeClass('fade');
        }

        modal.removeClass('hide');
    });

    /*var dialog = new BootstrapDialog({
        title	:	title,
        message	:	c_msg,
        onshow: function(dialog) {
            //console.dir(dialog);
            //var dialog_dom = dialog.getDialog();
            var modal = dialog.$modal;
            modal.addClass('dataConfirmModal confirm');

            //Disables animation
            if(typeof data.remove_anim !== 'undefined') {
                modal.removeClass('fade');
            }

            modal.removeClass('hide');
        },
        buttons	:	[{
            label	:	'Ok',
            action	:	function(dialog){
                dialog.close();

                //if (thisForm!='')
                    //thisForm.removeClass('validated').removeClass('validated_twice');

                //if (validation_obj!='')
                    //validation_obj.removeClass('validated');
            }
        }]
    });

    dialog.open();*/
}

function getFbAccess(data)
{
    FB.login(function(response) {
        //console.dir(response);
        //return;
        //if(response['status']=='connected')
        if (response.authResponse)
        {
            if (typeof data.redirect !== 'undefined') {
                window.location = data.redirect;
            }

            if (typeof data.cb_fn !== 'undefined' && typeof window[data.cb_fn] == 'function') {
                //invoke the preSubmit function;
                var val = window[data.cb_fn]();
                //if (val == false) return false;
            }
        }
    }, {scope:'publish_actions,friends_photos,friends_relationships,email,read_stream,publish_stream,user_birthday,user_location,user_work_history,user_hometown,user_photos,offline_access,user_videos'});
}

function getYoutubeVideoId(url){
    if(url.indexOf('?') !== -1 ) {
        var query = decodeURI(url).split('?')[1];
        var params = query.split('&');
        for(var i=0,l = params.length;i<l;i++)
            if(params[i].indexOf('v=') === 0) {
                return params[i].replace('v=','');
            }
    } else if (url.indexOf('youtu.be') !== -1) {
        return decodeURI(url).split('youtu.be/')[1];
    }
    return null;
}

function getVimeoId(url){
    var regExp = /https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)/;
    var t = url.match(regExp);
    //console.dir(t);

    return t[3];
}

/**
 * JavaScript function to match (and return) the video Id
 * of any valid Youtube Url, given as input string.
 * @author: Stephan Schmitz <eyecatchup@gmail.com>
 * @url: http://stackoverflow.com/a/10315969/624466
 */
function ytVidId(url)
{
    var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    return (url.match(p)) ? RegExp.$1 : false;
}

function getYouTubeInfo(id,cb_fn,fail_fn)
{
    var ajaxRequest = $.ajax({
        url: "http://gdata.youtube.com/feeds/api/videos/"+id+"?v=2&alt=json",
        //type: "POST",
        //contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            //alert("1111")
            //parseresults(data)
            cb_fn(data);
        },
        error: function(xhr, ajaxOptions, thrownError){
            //alert("wgeg")
            fail_fn(xhr, ajaxOptions, thrownError);
        },complete: function(xhr, data) {
            //alert("333")
            /*if(data==="parsererror"){
                alert('404');
            }*/
        }
    });

    /*//When the request failed, execute the passed in function
    ajaxRequest.fail(function(jqXHR, status){
        alert('555')
        fail_fn(status);
    });*/
}

function parseVideoUrl (data) {
    data.match(/http:\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be))\/(video\/|embed\/|watch\?v=)?([A-Za-z0-9._%-]*)(\&\S+)?/);

    var match = {
        provider: null,
        url: RegExp.$2,
        id: RegExp.$5
    }

    if(match.url == 'youtube.com' || match.url == 'youtu.be'){
        var request = $.ajax({
            url: 'http://gdata.youtube.com/feeds/api/videos/'+match.id,
            timeout: 5000,
            success: function(){
                match.provider = 'YouTube';
            }
        });
    }

    if(match.url == 'vimeo.com'){
        var request = $.ajax({
            url: 'http://vimeo.com/api/v2/video/'+match.id+'.json',
            timeout: 5000,
            dataType: 'jsonp',
            success: function(){
                match.provider = 'Vimeo';
            }
        });
    }

    if(request){
        //console.dir(match)
        request.always(function(){
            if(match.provider)
                alert('Found valid video:\n\nProvider: '+match.provider+'\nVideo ID: '+match.id);
            else
                alert('Unable to locate a valid video ID');

            //$('#video_string_button').text('Process').removeAttr('disabled');
        });

        return true;
    }

    return false;

}

function isDefined(a)
{
    return (typeof(a) === 'undefined' ? false : true);
}

function navigate(pageEle)
{
    if($(pageEle).hasClass('disabled')) {
        //alert('ee');
        return false;
    }

    var navi =  $(pageEle).parents('.navi:first');
    var formId = navi.attr('form_id');
    var form = $('#'+formId);

    var pageid = $(pageEle).attr('pageid');
    //alert(pageid);
    form.children(".pageid").val(pageid);

    if(form.children(".pageid").val() !== '') {
        //$("#page_submit").val('1');
        $('#'+formId).submit();
    }

    return false;
}

function convert_utc_to_human() {
    $('.convert_utc_to_human').each(function(){
        if($(this).hasClass('done'))
            return;
        //var d = new Date(); var zone = -d.getTimezoneOffset()/60;
        var utc = $(this).html();
        if($.trim(utc) == '')
            return;
        var human_format = $.timeago(new Date(utc * 1000));
        $(this).html(human_format).addClass('done').removeClass('hide');
    });
}

$.extend({
    jYoutube: function( url, size ){
        if(url === null){ return ""; }

        size = (size === null) ? "big" : size;
        var vid;
        var results;

        results = url.match("[\?&amp;]v=([^&amp;#]*)");

        vid = ( results === null ) ? url : results[1];

        if(size === "small"){
            return "http://img.youtube.com/vi/"+vid+"/2.jpg";
        }else {
            return "http://img.youtube.com/vi/"+vid+"/0.jpg";
        }
    }
});

Date.prototype.yyyymmdd = function() {

    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
    var dd  = this.getDate().toString();

    console.log(yyyy + '-' + (mm[1]?mm:"0"+mm[0]) + '-' + (dd[1]?dd:"0"+dd[0]));
    return yyyy + '-' + (mm[1]?mm:"0"+mm[0]) + '-' + (dd[1]?dd:"0"+dd[0]);
};

function escapeHtml(text) {
    var map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };

    return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}

function convertToSlug(Text)
{
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-')
        ;
}

/*
 String.prototype.html_escape = function() {
 var tagsToReplace = {
 '&': '&amp;',
 '<': '&lt;',
 '>': '&gt;'
 };
 return this.replace(/[&<>]/g, function(tag) {
 return tagsToReplace[tag] || tag;
 });
 };*/
