$(function() {
    init_site();
});

function init_site()
{
    var d = new Date();
    var tz = jstz.determine();

    var client_time = {};
    client_time.tz_name = '';
    client_time.date_is_dst = '';
    client_time.offset = 0;
    if (typeof(tz) !== 'undefined') {
        client_time.date_is_dst = jstz.date_is_dst(d);
        client_time.tz_name = tz.name();
    }

    //alert("weg")
    //console.dir(client_time);

    client_time.offset = -parseInt(d.getTimezoneOffset());

    var options = {
        URL:'/site/client_time',
        POST:client_time
    };
    almAjax(options);

    $('body').on('click','.forgot_pwd',function() {
        $('#frm_forgot_pwd').removeClass('hide').hide();
        $('#frm_site_login').fadeOut(200,function(){
            $('#frm_forgot_pwd').fadeIn('500');
        })
    });

    $('body').on('click','.back_to_login',function() {
        $('#frm_forgot_pwd').fadeOut(200,function(){
            $('#frm_site_login').fadeIn('500');
            $('#frm_forgot_pwd .frm_alert_hld').html('').addClass('hide');
            $('#frm_forgot_pwd .to-be-hidden').removeClass('hide');
            $('#frm_forgot_pwd .btn-submit').removeClass('hide');

        })
    });
}

/////////////FORM VALIDATIONS///////////////
function frm_register_validation(thisForm)
{
    var return_var = true;

    if(typeof thisForm.data('disabled') !== 'undefined') {
        return_var = false;
    }

    var email_ele = thisForm.children().find('.email');
    var pwd_ele = thisForm.children().find('.password');
    var r_pwd_ele = thisForm.children().find('.r_password');

    var email = email_ele.val();
    var pwd = pwd_ele.val();
    var r_pwd = r_pwd_ele.val();

    var error_list = [];
    var frm_alert_hld = thisForm.children().find('.frm_alert_hld');
    frm_alert_hld.html('');

    if(email === '') {
        error_list.push('Please enter your email.');
        add_frm_element_err(email_ele);
        return_var = false;
    }
    else if(!validateEmail(email)) {
        error_list.push('Please enter a valid email.');
        add_frm_element_err(email_ele);
        return_var = false;
    }
    else {
        remove_frm_element_err(email_ele);
    }

    if(pwd === '') {
        error_list.push('Please enter password.');
        add_frm_element_err(pwd_ele);
        return_var = false;
    } else {
        remove_frm_element_err(pwd_ele);
    }

    remove_frm_element_err(r_pwd_ele);
    if(r_pwd === '') {
        error_list.push('Please enter repeat password.');
        add_frm_element_err(r_pwd_ele);
        return_var = false;
    } else if(pwd !== '' && pwd !== r_pwd) {
        error_list.push('Password do not match.');
        add_frm_element_err(r_pwd_ele);
        return_var = false;
    }

    $.each(error_list,function(i,v){
        frm_alert_hld.append('<div class="alert alert-danger">'+v+'</div>');
    });

    frm_alert_hld.removeClass('hide');
    if(return_var) {
        frm_alert_hld.addClass('hide');
        thisForm.data('disabled',true);
    }

    return return_var;
}

function frm_login_validation(thisForm)
{
    var return_var = true;

    if(typeof thisForm.data('disabled') !== 'undefined') {
        return_var = false;
    }

    var email_ele = thisForm.children().find('.email');
    var pwd_ele = thisForm.children().find('.password');

    var email = email_ele.val();
    var pwd = pwd_ele.val();

    var error_list = [];
    var frm_alert_hld = thisForm.children().find('.frm_alert_hld');
    frm_alert_hld.html('');

    if(email === '') {
        error_list.push('Please enter your email or username.');
        add_frm_element_err(email_ele);
        return_var = false;
    /*} else if(!validateEmail(email)) {
        error_list.push('Please enter a valid email.');
        add_frm_element_err(email_ele);
        return_var = false;*/
    } else {
        remove_frm_element_err(email_ele);
    }

    if(pwd === '') {
        error_list.push('Please enter password.');
        add_frm_element_err(pwd_ele);
        return_var = false;
    } else {
        remove_frm_element_err(pwd_ele);
    }

    $.each(error_list,function(i,v){
        frm_alert_hld.append('<div class="alert alert-danger">'+v+'</div>');
    });

    frm_alert_hld.removeClass('hide');
    if(return_var) {
        thisForm.children().find('.btn-submit').html('<span class="semibold">Please wait..</span>');
        frm_alert_hld.addClass('hide');
        thisForm.data('disabled',true);
    }

    return return_var;
}

function frm_change_password_validation(thisForm)
{
    var return_var = true;

    if(typeof thisForm.data('disabled') !== 'undefined') {
        return_var = false;
    }

    var pwd_ele = thisForm.children().find('.password');
    var r_pwd_ele = thisForm.children().find('.r_password');

    var pwd = pwd_ele.val();
    var r_pwd = r_pwd_ele.val();

    var error_list = [];
    var frm_alert_hld = thisForm.children().find('.frm_alert_hld');
    frm_alert_hld.html('');

    if(pwd === '') {
        error_list.push('Please enter password.');
        add_frm_element_err(pwd_ele);
        return_var = false;
    } else {
        remove_frm_element_err(pwd_ele);
    }

    if(r_pwd === '') {
        error_list.push('Please retype password.');
        add_frm_element_err(r_pwd_ele);
        return_var = false;
    } else {
        remove_frm_element_err(r_pwd_ele);
    }

    if(pwd !== r_pwd) {
        error_list.push('Password do not match.');
        add_frm_element_err(pwd_ele);
        add_frm_element_err(r_pwd_ele);
        return_var = false;
    } else {
        remove_frm_element_err(pwd_ele);
        remove_frm_element_err(r_pwd_ele);
    }

    $.each(error_list,function(i,v){
        frm_alert_hld.append('<div class="alert alert-danger">'+v+'</div>');
    });

    frm_alert_hld.removeClass('hide');
    if(return_var) {
        thisForm.children().find('.btn-submit').html('<span class="semibold">Please wait..</span>');
        frm_alert_hld.addClass('hide');
        thisForm.data('disabled',true);
    }

    return return_var;
}

//////////CALLBACKS/////////////////
function AMcallBack_site_user_register(data)
{
    //console.dir(data);
    if(data.DATA.success) {
        //alert("wegewg")
        //$('.cur_user_prof_pic').attr('src',data.DATA.profile_thumb_pic);
        window.location=data.DATA.redirect;
    }
    else {
        theme_form_err_msg(data.DATA.error_msg, '#frm_site_register');

        $('#frm_site_register').removeData('disabled');
    }
}



function AMcallBack_site_user_login(data)
{
    //console.dir(data);
    if(data.DATA.success) {
        //alert("wegewg")
        //$('.cur_user_prof_pic').attr('src',data.DATA.profile_thumb_pic);
        window.location=data.DATA.redirect;
    }
    else {
        var filt_data = {};
        filt_data.msg_before = '<div class="alert alert-danger">';
        theme_form_err_msg(data.DATA.error_msg, '#frm_site_login', filt_data);

        $('#frm_site_login .btn-submit').html('<span class="semibold">Login</span>');
        $('#frm_site_login').removeData('disabled');
    }
}

function AMcallBack_site_forgot_pwd(data)
{
    //console.dir(data);
    if(data.DATA.success) {
        //Check your inbox and click the link in the email to reset your password
        var succes_alert ="<div class='alert alert-success ma5'>We've sent a reset link to your email.</div>";
        $('#frm_forgot_pwd input[type="text"]').val('');
        $('#frm_forgot_pwd .frm_alert_hld').html(succes_alert).removeClass('hide');
        $('#frm_forgot_pwd .to-be-hidden').addClass('hide');
        $('#frm_forgot_pwd .btn-submit').addClass('hide');


    }
    else {
        //theme_form_err_msg(data.DATA.error_msg, '#frm_forgot_pwd');
        //console.log(data.DATA.error_msg['']);
        var error_alert ="<div class='alert alert-danger ma5'>Please Enter Proper Email Address</div>";
        $('#frm_forgot_pwd input[type="text"]').val('');
        $('#frm_forgot_pwd .frm_alert_hld').html(error_alert).removeClass('hide');

        $('#frm_forgot_pwd .btn-submit').html('<span class="semibold">Send The Reset Link</span>');
        $('#frm_forgot_pwd').removeData('disabled');
    }
}

function AMcallBack_site_change_password(data)
{
    if(data.DATA.is_reset_pwd) {
        $('#frm_change_password .btn-submit').html('<span class="semibold">Reset Password</span>');
    } else {
        $('#frm_change_password .btn-submit').html('<span class="semibold">Create Password</span>');
    }
    //console.dir(data);
    if(data.DATA.success) {
        if(data.DATA.is_reset_pwd) {
            var succes_alert ="<div class='alert alert-success ma5_'>Password has been successfully changed. Please <a href='/'>Login</a> to continue.</div>";
            //$('#frm_change_password input[type="password"]').val('');
            //$('#frm_change_password .form-group').addClass('hide');
            //$('#frm_change_password .back_to_login_hld').removeClass('hide');
            $('#frm_change_password').addClass('hide');
        } else {
            var succes_alert ="<div class='alert alert-success ma5'>Password has been created successfully. Please <a href='/'>Login</a> to continue.</div>";
            $('#frm_change_password').addClass('hide');
        }
        $('#frm_change_password').before(succes_alert);
    }
    else {
        theme_form_err_msg(data.DATA.error_msg, '#frm_change_password');

        $('#frm_change_password').removeData('disabled');
    }
}

